/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <linux/ethtool.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <errno.h>
#include <linux/sockios.h>

#include "mod_eth_common.h"
#include "mod_eth_ethtool.h"

#define ME "eth-utils"

#if defined ETHTOOL_GLINKSETTINGS && defined ETHTOOL_SLINKSETTINGS
#define ETHTOOL_LINK_MODE_MASK_MAX_KERNEL_NU32 (SCHAR_MAX)
struct ethtool_link_usettings {
    struct ethtool_link_settings base;

    struct {
        uint32_t supported[ETHTOOL_LINK_MODE_MASK_MAX_KERNEL_NU32];
        uint32_t advertising[ETHTOOL_LINK_MODE_MASK_MAX_KERNEL_NU32];
        uint32_t lp_advertising[ETHTOOL_LINK_MODE_MASK_MAX_KERNEL_NU32];
    } link_modes;
};

static struct _capabilities {
    uint32_t caps;
    int32_t speed;
    uint8_t mode;
} capabilities[] = {
    {ETHTOOL_LINK_MODE_10baseT_Half_BIT, 10, DUPLEX_HALF},
    {ETHTOOL_LINK_MODE_10baseT_Full_BIT, 10, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100baseT_Half_BIT, 100, DUPLEX_HALF},
    {ETHTOOL_LINK_MODE_100baseT_Full_BIT, 100, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_1000baseT_Half_BIT, 1000, DUPLEX_HALF},
    {ETHTOOL_LINK_MODE_1000baseT_Full_BIT, 1000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseT_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_2500baseX_Full_BIT, 2500, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_1000baseKX_Full_BIT, 1000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseKX4_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseKR_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseR_FEC_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_20000baseMLD2_Full_BIT, 20000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_20000baseKR2_Full_BIT, 20000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_40000baseKR4_Full_BIT, 40000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_40000baseCR4_Full_BIT, 40000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_40000baseSR4_Full_BIT, 40000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_40000baseLR4_Full_BIT, 40000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_56000baseKR4_Full_BIT, 56000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_56000baseCR4_Full_BIT, 56000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_56000baseSR4_Full_BIT, 56000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_56000baseLR4_Full_BIT, 56000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_25000baseCR_Full_BIT, 25000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_25000baseKR_Full_BIT, 25000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_25000baseSR_Full_BIT, 25000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseCR2_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseKR2_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseKR4_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseSR4_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseCR4_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseLR4_ER4_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseSR2_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_1000baseX_Full_BIT, 1000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseCR_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseSR_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseLR_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseLRM_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseER_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_2500baseT_Full_BIT, 2500, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_5000baseT_Full_BIT, 5000, DUPLEX_FULL},
#ifdef ETHTOOL_LINK_MODE_50000baseKR_Full_BIT
    {ETHTOOL_LINK_MODE_50000baseKR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseSR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseCR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseLR_ER_FR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseDR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseKR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseSR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseCR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseLR2_ER2_FR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseDR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseKR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseSR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseLR4_ER4_FR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseDR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseCR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100baseT1_Full_BIT, 100, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_1000baseT1_Full_BIT, 1000, DUPLEX_FULL},
#endif
};
static size_t nr_capabilities = sizeof(capabilities) / sizeof(capabilities[0]);
#else
#define LEGACY_ETHTOOL_MODE
#endif

/**
 * @brief Returns the highest supported mode for legacy mode
 */
static int32_t ethernet_ethtool_get_biterate(const struct ethtool_cmd* ecmd) {
    SAH_TRACEZ_IN(ME);
    struct _supported_capabilities {
        uint32_t caps;
        int32_t speed;
    } supported_capabilities[] = {
        {SUPPORTED_10baseT_Half, 10},
        {SUPPORTED_10baseT_Full, 10},
        {SUPPORTED_100baseT_Half, 100},
        {SUPPORTED_100baseT_Full, 100},
        {SUPPORTED_1000baseT_Half, 1000},
        {SUPPORTED_1000baseT_Full, 1000},
        {SUPPORTED_10000baseT_Full, 1000},
        {SUPPORTED_2500baseX_Full, 2500},
        {SUPPORTED_1000baseKX_Full, 1000},
        {SUPPORTED_10000baseKX4_Full, 10000},
        {SUPPORTED_10000baseKR_Full, 10000},
        {SUPPORTED_10000baseR_FEC, 10000},
        {SUPPORTED_20000baseMLD2_Full, 20000},
        {SUPPORTED_20000baseKR2_Full, 20000},
#ifdef SUPPORTED_40000baseKR4_Full
        {SUPPORTED_40000baseKR4_Full, 40000},
        {SUPPORTED_40000baseCR4_Full, 40000},
        {SUPPORTED_40000baseSR4_Full, 40000},
        {SUPPORTED_40000baseLR4_Full, 40000},
#endif
#ifdef SUPPORTED_56000baseKR4_Full
        {SUPPORTED_56000baseKR4_Full, 56000},
        {SUPPORTED_56000baseCR4_Full, 56000},
        {SUPPORTED_56000baseSR4_Full, 56000},
        {SUPPORTED_56000baseLR4_Full, 56000},
#endif
    };
    size_t nr_supported_capabilities = sizeof(supported_capabilities) / sizeof(supported_capabilities[0]);
    int i = 0;
    int32_t speed = -1;

    for(i = nr_supported_capabilities - 1; i >= 0; i--) {
        if(ecmd->supported & supported_capabilities[i].caps) {
            return supported_capabilities[i].speed;
        }
    }
    SAH_TRACEZ_OUT(ME);
    return speed;
}

/**
 * @brief Legacy ioctl implementation to get/set settings
 * @param cmd ETHTOOL_GSET for get or ETHTOOL_SSET for set
 * @param name interface name for the interface the settings should be set
 * @param ecmd ethtool cmd struct containing the settings
 * @return returns 0 if function was successful, -1 otherwise
 */
static int ethernet_ethtool_ioctl(uint32_t cmd, const char* name, struct ethtool_cmd* ecmd) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ifreq ifr;
    int fd = -1;

    when_null(ecmd, exit);
    when_str_empty(name, exit);

    memset(&ifr, 0, sizeof(ifr));
    fd = ioctl_fd_get();
    when_false_trace(fd > 0, exit, ERROR, "No valid file descriptor");

    ecmd->cmd = cmd;

    ifr.ifr_data = (char*) ecmd;
    strncpy(ifr.ifr_name, name, IF_NAMESIZE);
    ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
    when_failed_trace(ioctl(fd, SIOCETHTOOL, &ifr), exit, ERROR, "ioctl call failed - %s", strerror(errno));
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Legacy implementation that reads the duplex mode and speed for the interface with the given name
 * @param name interface name for the interface the settings should be read
 * @param duplex_mode_index unsigned char pointer in which the duplex mode index will be stored
 * @param speed unsigned integer pointer in which the current bit rate will be stored
 * @return returns 0 if function was successful, -1 otherwise
 */
static int ethernet_ethtool_get(const char* name, unsigned char* duplex_mode_index, uint32_t* speed) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ethtool_cmd ecmd;

    memset(&ecmd, 0, sizeof(ecmd));

    when_str_empty_trace(name, exit, ERROR, "Interface name is required to read settings");

    when_failed_trace(ethernet_ethtool_ioctl(ETHTOOL_GSET, name, &ecmd), exit, INFO, "Failed to get link settings for '%s'", name);
    when_false_trace(ecmd.duplex <= 2, exit, ERROR, "Malformed value returned by ioctl");
    *speed = ecmd.speed;
    *duplex_mode_index = ecmd.duplex;
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Legacy implementation that sets the duplex mode and speed for the interface with the given name
 * @param name interface name for the interface the settings should be set
 * @param speed desired bit rate
 * @param duplex desired duplex mode "Full" or "Half"
 * @return returns 0 if function was successful, -1 otherwise
 */
static int ethernet_ethtool_set(const char* name, int32_t speed, const char* duplex) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ethtool_cmd ecmd;

    when_str_empty(name, exit);

    memset(&ecmd, 0, sizeof(ecmd));
    rv = ethernet_ethtool_ioctl(ETHTOOL_GSET, name, &ecmd);
    when_failed_trace(rv, exit, INFO, "Failed to perform GSET on '%s'", name);
    when_false_trace((ecmd.duplex <= 2), exit, ERROR, "ioctl returned a malformed value %d", ecmd.duplex);

    rv = -1;
    ecmd.duplex = DUPLEX_UNKNOWN;
    if(0 == strcmp("Full", duplex)) {
        ecmd.duplex = DUPLEX_FULL;
    } else if(0 == strcmp("Half", duplex)) {
        ecmd.duplex = DUPLEX_HALF;
    }

    switch(speed) {
    case SPEED_UNKNOWN:     // -1
        speed = ethernet_ethtool_get_biterate(&ecmd);
        when_false_trace(speed > 0, exit, ERROR, "Unable to find a valid speed");
        ethtool_cmd_speed_set(&ecmd, speed);
        break;
    case 10:
    case 100:
    case 1000:
    case 2500:
    case 5000:
    case 10000:
    case 14000:
    case 20000:
    case 25000:
    case 40000:
    case 50000:
    case 56000:
    case 100000:
        ethtool_cmd_speed_set(&ecmd, speed);
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Can not change the configuration, MaxBitRate out of specification");
        goto exit;
    }

    rv = ethernet_ethtool_ioctl(ETHTOOL_SSET, name, &ecmd);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

#if !defined(LEGACY_ETHTOOL_MODE)

/**
 * @brief ioctl implementation to get/set the linksettings
 * @param cmd ETHTOOL_GLINKSETTINGS for get or ETHTOOL_SLINKSETTINGS for set
 * @param name interface name for the interface the settings should be set
 * @param elscmd ethtool linksetttings struct containing the settings
 * @return returns 0 if function was successful, -1 otherwise
 */
static int ethernet_ethtool_linksettings_ioctl(uint32_t cmd, const char* name, struct ethtool_link_usettings* elscmd) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ifreq ifr;
    int fd = -1;
    struct {
        struct ethtool_link_settings req;
        // Make link_mode_data big enough for supported, advertising and lp_advertising data.
        // Each gets a size of ETHTOOL_LINK_MODE_MASK_MAX_KERNEL_NU32
        uint32_t link_mode_data[3 * ETHTOOL_LINK_MODE_MASK_MAX_KERNEL_NU32];
    } ecmd;
    uint32_t offset = 0;

    when_null_trace(elscmd, exit, ERROR, "User settings structure is NULL");
    when_str_empty_trace(name, exit, ERROR, "Name of the interface is NULL");
    when_true_trace((cmd != ETHTOOL_GLINKSETTINGS) && (cmd != ETHTOOL_SLINKSETTINGS), exit, ERROR, "Wrong cmd parameter, should be ETHTOOL_GLINKSETTINGS or ETHTOOL_SLINKSETTINGS");

    memset(&ifr, 0, sizeof(ifr));
    memset(&ecmd, 0, sizeof(ecmd));

    fd = ioctl_fd_get();
    when_false_trace(fd > 0, exit, ERROR, "No valid file descriptor");

    strncpy(ifr.ifr_name, name, IF_NAMESIZE);
    ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
    if(cmd == ETHTOOL_GLINKSETTINGS) {
        ecmd.req.cmd = ETHTOOL_GLINKSETTINGS;
        ifr.ifr_data = (char*) &ecmd;
        when_true_trace((ioctl(fd, SIOCETHTOOL, &ifr) == -1), exit, ERROR, "ioctl call ETHTOOL_GLINKSETTINGS failed, first call- %s - for device name %s", strerror(errno), ifr.ifr_name);
        when_true_trace((ecmd.req.link_mode_masks_nwords >= 0) || (ecmd.req.cmd != ETHTOOL_GLINKSETTINGS), exit, ERROR, "ioctl call failed with mask words not negative- %s", strerror(errno));
        ecmd.req.cmd = cmd;
        ecmd.req.link_mode_masks_nwords = -ecmd.req.link_mode_masks_nwords;
        ifr.ifr_data = (char*) &ecmd;
        when_true_trace((ioctl(fd, SIOCETHTOOL, &ifr) == -1), exit, ERROR, "ioctl call ETHTOOL_GLINKSETTINGS failed, second call- %s - for device name %s", strerror(errno), ifr.ifr_name);
        when_true_trace((ecmd.req.link_mode_masks_nwords <= 0) || (ecmd.req.cmd != ETHTOOL_GLINKSETTINGS), exit, ERROR, "ioctl call failed with mask words not negative- %s", strerror(errno));

        memcpy(&(elscmd->base), &ecmd.req, sizeof(struct ethtool_link_settings));
        offset = 0;
        memcpy(elscmd->link_modes.supported, &ecmd.link_mode_data[offset], 4 * ecmd.req.link_mode_masks_nwords);
        offset += ecmd.req.link_mode_masks_nwords;
        memcpy(elscmd->link_modes.advertising, &ecmd.link_mode_data[offset], 4 * ecmd.req.link_mode_masks_nwords);
        offset += ecmd.req.link_mode_masks_nwords;
        memcpy(elscmd->link_modes.lp_advertising, &ecmd.link_mode_data[offset], 4 * ecmd.req.link_mode_masks_nwords);

    } else {
        memcpy(&ecmd.req, &elscmd->base, sizeof(ecmd.req));
        ecmd.req.cmd = ETHTOOL_SLINKSETTINGS;

        offset = 0;
        memcpy(&ecmd.link_mode_data[offset], elscmd->link_modes.supported, 4 * ecmd.req.link_mode_masks_nwords);
        offset += ecmd.req.link_mode_masks_nwords;
        memcpy(&ecmd.link_mode_data[offset], elscmd->link_modes.advertising, 4 * ecmd.req.link_mode_masks_nwords);
        offset += ecmd.req.link_mode_masks_nwords;
        memcpy(&ecmd.link_mode_data[offset], elscmd->link_modes.lp_advertising, 4 * ecmd.req.link_mode_masks_nwords);

        ifr.ifr_data = (char*) &ecmd;
        when_true_trace((ioctl(fd, SIOCETHTOOL, &ifr) == -1), exit, ERROR,
                        "ioctl call failed to set parameters to %s - %s",
                        ifr.ifr_name, strerror(errno));
    }
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Reads the duplex mode and speed for the interface with the given name
 * @param name interface name for the interface the settings should be read
 * @param speed unsigned integer pointer in which the bit rate will be stored
 * @param duplex_mode_index unsigned char pointer in which the duplex mode index will be stored
 * @return returns 0 if function was successful, -1 otherwise
 */
static int ethernet_ethtool_linksettings_get(const char* name, uint32_t* speed, unsigned char* duplex_mode_index) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ethtool_link_usettings elscmd;

    memset(&elscmd, 0, sizeof(elscmd));
    rv = ethernet_ethtool_linksettings_ioctl(ETHTOOL_GLINKSETTINGS, name, &elscmd);
    if(rv != 0) {
        SAH_TRACEZ_INFO(ME, "ETHTOOL_GLINKSETTINGS not supported, falling back to ETHTOOL_GSET");
        rv = ethernet_ethtool_get(name, duplex_mode_index, speed);
        when_failed_trace(rv, exit, INFO, "Failed to get duplex mode and speed using SET commands");
    } else {
        SAH_TRACEZ_INFO(ME, "ETHTOOL_GLINKSETTINGS supported");
        when_false_trace(elscmd.base.duplex <= 2, exit, ERROR, "Malformed value returned by ioctl");
        *speed = elscmd.base.speed;
        *duplex_mode_index = elscmd.base.duplex;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Sets the duplex mode and speed for the interface with the given name
 * @param name interface name for the interface the settings should be set
 * @param speed desired max bit rate
 * @param duplex desired duplex mode "Auto", "Full" or "Half"
 * @param elscmd ethtool linksetttings struct containing the current settings, received from a ETHTOOL_GLINKSETTINGS
 * @return returns 0 if function was successful, -1 otherwise
 */
static int ethernet_ethtool_linksettings_set(const char* name, int32_t speed, const char* duplex, struct ethtool_link_usettings elscmd) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    size_t i = 0;
    uint8_t duplex_mode = DUPLEX_UNKNOWN;
    bool auto_mode = false;

    when_str_empty_trace(name, exit, ERROR, "Missing interface name");
    when_str_empty_trace(duplex, exit, ERROR, "Duplex mode can not be empty");

    if(0 == strcmp("Auto", duplex)) {
        auto_mode = true;
    } else if(0 == strcmp("Full", duplex)) {
        duplex_mode = DUPLEX_FULL;
    } else if(0 == strcmp("Half", duplex)) {
        duplex_mode = DUPLEX_HALF;
    }

    if(speed < 0) {
        //A value of -1 indicates automatic selection of the maximum bit rate.
        // So all possible bit rates should be advertised
        speed = INT32_MAX;
    }

    for(i = 0; i < nr_capabilities; i++) {
        if((capabilities[i].speed <= speed) && (auto_mode || (capabilities[i].mode == duplex_mode))) {
            // set link mode bit
            elscmd.link_modes.advertising[capabilities[i].caps / 32] |= (1UL << ((capabilities[i].caps) % 32));
        } else {
            // clear link mode bit
            elscmd.link_modes.advertising[capabilities[i].caps / 32] &= ~(1UL << ((capabilities[i].caps) % 32));
        }
    }

    elscmd.base.autoneg = AUTONEG_ENABLE; // Make sure to enable the auto negotiation if not setting a single fixed speed
    rv = ethernet_ethtool_linksettings_ioctl(ETHTOOL_SLINKSETTINGS, name, &elscmd);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Will try to set the speed and duplex mode for the given interface using linksettings. If this fails it will fall back on legacy mode
 * @param name interface name for the interface the settings should be set
 * @param speed desired max bit rate
 * @param duplex desired duplex mode "Auto", "Full" or "Half"
 * @return returns 0 if function was successful, -1 otherwise
 */
static int ethernet_ethtool_settings_set(const char* name, int32_t speed, const char* duplex) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ethtool_link_usettings elscmd;

    when_str_empty_trace(name, exit, ERROR, "Interface name can not be empty");

    memset(&elscmd, 0, sizeof(elscmd));
    rv = ethernet_ethtool_linksettings_ioctl(ETHTOOL_GLINKSETTINGS, name, &elscmd);
    if(rv != 0) {
        SAH_TRACEZ_INFO(ME, "ETHTOOL_GLINKSETTINGS not supported, falling back to ETHTOOL_GSET");
        rv = ethernet_ethtool_set(name, speed, duplex);
    } else {
        SAH_TRACEZ_INFO(ME, "ETHTOOL_GLINKSETTINGS supported");
        rv = ethernet_ethtool_linksettings_set(name, speed, duplex, elscmd);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
#endif

int link_settings_get(UNUSED const char* function_name,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* intf_name = GET_CHAR(args, "intf_name");
    const char* duplex_mode[3] = {"Half", "Full", "Unknown"};
    unsigned char duplex_mode_index = 0;
    uint32_t speed = 0;

    when_str_empty_trace(intf_name, exit, ERROR, "Interface name is required to read settings");

#if !defined(LEGACY_ETHTOOL_MODE)
    rv = ethernet_ethtool_linksettings_get(intf_name, &speed, &duplex_mode_index);
    when_failed_trace(rv, exit, INFO, "Failed to get duplex mode and speed");
#else
    rv = ethernet_ethtool_get(intf_name, &duplex_mode_index, &speed);
    when_failed_trace(rv, exit, INFO, "Failed to get duplex mode and speed using SET commands");
#endif

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int32_t, ret, "current_bit_rate", speed);
    if(duplex_mode_index > 2) {
        duplex_mode_index = 2;
    }
    amxc_var_add_key(cstring_t, ret, "current_duplex_mode", duplex_mode[duplex_mode_index]);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int link_settings_set(UNUSED const char* function_name,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* intf_name = GET_CHAR(args, "intf_name");
    const char* duplex_mode = GET_CHAR(args, "duplex_mode");
    int32_t max_bitrate = GET_INT32(args, "max_bit_rate");

    when_str_empty_trace(intf_name, exit, ERROR, "Interface name can not be empty");
    when_str_empty_trace(duplex_mode, exit, ERROR, "Duplex mode can not be empty");

#if !defined(LEGACY_ETHTOOL_MODE)
    rv = ethernet_ethtool_settings_set(intf_name, max_bitrate, duplex_mode);
#else
    rv = ethernet_ethtool_set(intf_name, max_bitrate, duplex_mode);
#endif

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

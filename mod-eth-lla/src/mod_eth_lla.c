/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <sys/sysinfo.h>
#include <errno.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_eth_common.h"
#include "mod_eth_ethtool.h"
#include "mod_eth_mac.h"
#include "mod_eth_mtu.h"
#include "mod_eth_lla.h"

#define ME "ethernet"

static int ethernet_enable_set(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* intf_name = GET_CHAR(args, "intf_name");
    bool enable = GET_BOOL(args, "enable");
    struct ifreq ifr;

    memset(&ifr, 0, sizeof(ifr));
    ifr.ifr_flags = 0;

    when_str_empty_trace(intf_name, exit, ERROR, "No netdev interface name given, invalid argument");

    strncpy(ifr.ifr_name, intf_name, IF_NAMESIZE);
    ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
    SAH_TRACEZ_INFO(ME, "Set \"%s\" %s", intf_name, enable ? "up" : "down");

    ioctl_intf_enable(ifr, enable);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int link_disable_arp(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* intf_name = GET_CHAR(args, "intf_name");
    bool noarp = GET_BOOL(args, "noarp");
    struct ifreq ifr;

    memset(&ifr, 0, sizeof(ifr));

    when_str_empty_trace(intf_name, exit, ERROR, "No netdev interface name given, invalid argument");

    strncpy(ifr.ifr_name, intf_name, IF_NAMESIZE);
    ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
    SAH_TRACEZ_INFO(ME, "Set \"%s\" %s", intf_name, noarp ? "NoARP" : "ARP");

    when_false_trace(ioctl_fd_get() > 0, exit, ERROR, "no valid file descriptor");

    when_failed_trace(ioctl(ioctl_fd_get(), SIOCGIFFLAGS, &ifr), exit, ERROR, "Get flags failed for %s: %s", ifr.ifr_name, strerror(errno));

    if(noarp) {
        ifr.ifr_flags |= IFF_NOARP;
    } else {
        ifr.ifr_flags &= ~IFF_NOARP;
    }

    when_failed_trace(ioctl(ioctl_fd_get(), SIOCSIFFLAGS, &ifr), exit, ERROR, "Set flags failed for %s: %s", ifr.ifr_name, strerror(errno));
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int system_uptime_get(UNUSED const char* function_name,
                             UNUSED amxc_var_t* args,
                             amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct sysinfo info;

    rv = sysinfo(&info);
    when_failed_trace(rv, exit, ERROR, "Could not read total uptime of the system");

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "uptime", info.uptime);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static AMXM_CONSTRUCTOR module_start(void) {
    SAH_TRACEZ_IN(ME);
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_ETH_LLA_CTRL);
    amxm_module_add_function(mod, "mac-address-get", mac_address_get);
    amxm_module_add_function(mod, "mac-address-set", mac_address_set);
    amxm_module_add_function(mod, "ethernet-enable-set", ethernet_enable_set);
    amxm_module_add_function(mod, "link-disable-arp", link_disable_arp);
    amxm_module_add_function(mod, "link-settings-get", link_settings_get);
    amxm_module_add_function(mod, "link-settings-set", link_settings_set);
    amxm_module_add_function(mod, "system-uptime-get", system_uptime_get);
    amxm_module_add_function(mod, "mtu-set", mtu_set);

    ioctl_fd_open();

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_DESTRUCTOR module_stop(void) {
    SAH_TRACEZ_IN(ME);

    ioctl_fd_close();

    SAH_TRACEZ_OUT(ME);
    return 0;
}

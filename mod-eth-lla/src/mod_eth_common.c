/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/socket.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_eth_common.h"

#define ME "ethernet"

static int ioctlfd = -1;

int ioctl_fd_open(void) {
    ioctlfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(ioctlfd < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not get a valid file descriptor");
    }
    return ioctlfd;
}

int ioctl_fd_get(void) {
    return ioctlfd;
}

void ioctl_fd_close(void) {
    close(ioctlfd);
    ioctlfd = -1;
}

bool intf_is_ethernet(struct ifreq ifr) {
    SAH_TRACEZ_IN(ME);
    bool is_ethernet = false;
    ifr.ifr_hwaddr.sa_family = 0;

    when_false_trace(ioctl_fd_get() > 0, exit, ERROR, "no valid file descriptor");

    if(ioctl(ioctl_fd_get(), SIOCGIFHWADDR, &ifr) < 0) {
        SAH_TRACEZ_ERROR(ME, "%s, failed to get mac address for interface %s", strerror(errno), ifr.ifr_name);
        goto exit;
    }

    is_ethernet = (ifr.ifr_hwaddr.sa_family == ARPHRD_ETHER);
    // make sure that the interface is an ethernet interface
    // otherwise a mac address can not be set
    when_false_trace(is_ethernet, exit, ERROR, "%s is not an ethernet interface", ifr.ifr_name);

exit:
    SAH_TRACEZ_OUT(ME);
    return is_ethernet;
}

bool ioctl_intf_enable(struct ifreq ifr, bool enable) {
    SAH_TRACEZ_IN(ME);
    bool was_enabled = false;

    when_false_trace(ioctl_fd_get() > 0, exit, ERROR, "no valid file descriptor");

    if(ioctl(ioctl_fd_get(), SIOCGIFFLAGS, &ifr) == -1) {
        SAH_TRACEZ_ERROR(ME, "Get flags failed for %s: %s", ifr.ifr_name, strerror(errno));
        goto exit;
    }
    if((ifr.ifr_flags & IFF_UP) != 0) {
        was_enabled = true;
    }

    if(enable) {
        ifr.ifr_flags |= IFF_UP;
    } else {
        ifr.ifr_flags &= ~IFF_UP;
    }

    if(ioctl(ioctl_fd_get(), SIOCSIFFLAGS, &ifr) == -1) {
        SAH_TRACEZ_ERROR(ME, "Set flags failed for %s: %s", ifr.ifr_name, strerror(errno));
        goto exit;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return was_enabled;
}
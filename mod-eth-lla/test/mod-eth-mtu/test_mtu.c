/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "../../include_priv/mod_eth_lla.h"

#include "test_mtu.h"
#define MOD_NAME "target_module"

int test_setup(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, MOD_NAME, "../modules_under_test/target_module.so"), 0);

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(amxm_close_all(), 0);
    return 0;
}

static int mock_read(const char* path) {
    int rv = -1;
    FILE* fd = fopen(path, "r");
    char buf[32] = {0};

    assert_non_null(fd);
    assert_non_null(fgets(buf, sizeof(buf), fd));
    rv = atoi(buf);
    fclose(fd);

    return rv;
}

void test_mtu_set(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    // try to set without the intf_name parameter set
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mtu-set", &var, &ret), -1);

    // Set the name but not the mtu size, should still fail
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "eth0");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mtu-set", &var, &ret), -1);

    // Now also set the mtu size
    amxc_var_add_key(uint32_t, &var, "mtu_size", 2000);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mtu-set", &var, &ret), 0);
    amxc_var_clean(&var);
    amxc_var_clean(&ret);

    assert_int_equal(mock_read("mock_files/eth0/sys_mtu"), 2000);
}

void test_mtu_set_ra(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    // Set the name but not the mtu size, should still fail
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "eth1");
    amxc_var_add_key(uint32_t, &var, "mtu_size", 1500);
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mtu-set", &var, &ret), 0);
    amxc_var_clean(&var);
    amxc_var_clean(&ret);

    assert_int_equal(mock_read("mock_files/eth1/sys_mtu"), 1500);
    assert_int_equal(mock_read("mock_files/eth1/proc_mtu"), 1000);

}

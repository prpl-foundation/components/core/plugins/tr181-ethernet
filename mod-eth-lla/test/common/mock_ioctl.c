/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <dlfcn.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <linux/ethtool.h>
#include <linux/sockios.h>
#include <setjmp.h>
#include <cmocka.h>

static char* mac_address = NULL;
static struct _capabilities {
    uint32_t caps;
    int32_t speed;
    uint8_t mode;
} capabilities[] = {
    {ETHTOOL_LINK_MODE_10baseT_Half_BIT, 10, DUPLEX_HALF},
    {ETHTOOL_LINK_MODE_10baseT_Full_BIT, 10, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100baseT_Half_BIT, 100, DUPLEX_HALF},
    {ETHTOOL_LINK_MODE_100baseT_Full_BIT, 100, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_1000baseT_Half_BIT, 1000, DUPLEX_HALF},
    {ETHTOOL_LINK_MODE_1000baseT_Full_BIT, 1000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseT_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_2500baseX_Full_BIT, 2500, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_1000baseKX_Full_BIT, 1000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseKX4_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseKR_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseR_FEC_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_20000baseMLD2_Full_BIT, 20000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_20000baseKR2_Full_BIT, 20000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_40000baseKR4_Full_BIT, 40000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_40000baseCR4_Full_BIT, 40000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_40000baseSR4_Full_BIT, 40000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_40000baseLR4_Full_BIT, 40000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_56000baseKR4_Full_BIT, 56000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_56000baseCR4_Full_BIT, 56000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_56000baseSR4_Full_BIT, 56000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_56000baseLR4_Full_BIT, 56000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_25000baseCR_Full_BIT, 25000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_25000baseKR_Full_BIT, 25000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_25000baseSR_Full_BIT, 25000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseCR2_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseKR2_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseKR4_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseSR4_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseCR4_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseLR4_ER4_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseSR2_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_1000baseX_Full_BIT, 1000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseCR_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseSR_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseLR_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseLRM_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_10000baseER_Full_BIT, 10000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_2500baseT_Full_BIT, 2500, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_5000baseT_Full_BIT, 5000, DUPLEX_FULL},
#ifdef ETHTOOL_LINK_MODE_50000baseKR_Full_BIT
    {ETHTOOL_LINK_MODE_50000baseKR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseSR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseCR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseLR_ER_FR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_50000baseDR_Full_BIT, 50000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseKR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseSR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseCR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseLR2_ER2_FR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100000baseDR2_Full_BIT, 100000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseKR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseSR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseLR4_ER4_FR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseDR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_200000baseCR4_Full_BIT, 200000, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_100baseT1_Full_BIT, 100, DUPLEX_FULL},
    {ETHTOOL_LINK_MODE_1000baseT1_Full_BIT, 1000, DUPLEX_FULL},
#endif
};
static size_t nr_capabilities = sizeof(capabilities) / sizeof(capabilities[0]);

int __wrap_ioctl(int d, unsigned long request, struct ifreq* ifr);
int __real_ioctl(int d, unsigned long request, struct ifreq* ifr);

struct ethtool_link_usettings {
    struct ethtool_link_settings req;
    uint32_t link_mode_data[3 * SCHAR_MAX];
};

static int test_link_settings(int32_t speed, uint8_t mode, const uint32_t advertising[]) {
    int rv = -1;
    int i = 0;
    int max_speed = -1;

    // Iterate through capabilities in reverse order
    for(i = nr_capabilities - 1; i >= 0; i--) {
        if(advertising[capabilities[i].caps / 32] & (1UL << (capabilities[i].caps % 32))) {
            if(capabilities[i].speed > speed) {
                goto exit;
            }
            if((mode != DUPLEX_UNKNOWN) && (capabilities[i].mode != mode)) {
                goto exit;
            }
            if(capabilities[i].speed > max_speed) {
                max_speed = capabilities[i].speed;
            }
        }
    }
    if(max_speed == speed) {
        rv = 0;
    }

exit:
    return rv;
}

int __wrap_ioctl(int d, unsigned long request, struct ifreq* ifr) {
    int rv = -1;

    if(request == SIOCETHTOOL) {
        struct ethtool_link_usettings* ecmd;
        ecmd = (struct ethtool_link_usettings*) ifr->ifr_data;

        if(ecmd->req.cmd == ETHTOOL_SLINKSETTINGS) {
            uint32_t offset = ecmd->req.link_mode_masks_nwords;
            assert_int_equal(ecmd->req.autoneg, AUTONEG_ENABLE);
            // Depending on the name, change the expected values
            if(strcmp(ifr->ifr_name, "1000.FULL") == 0) {
                assert_int_equal(test_link_settings(1000, DUPLEX_FULL, &ecmd->link_mode_data[offset]), 0);
                rv = 0;
            } else if(strcmp(ifr->ifr_name, "100.HALF") == 0) {
                assert_int_equal(test_link_settings(100, DUPLEX_HALF, &ecmd->link_mode_data[offset]), 0);
                rv = 0;
            } else if(strcmp(ifr->ifr_name, "5000.AUTO") == 0) {
                assert_int_equal(test_link_settings(5000, DUPLEX_UNKNOWN, &ecmd->link_mode_data[offset]), 0);
                rv = 0;
            }
        } else if(ecmd->req.cmd == ETHTOOL_GLINKSETTINGS) {
            if((strcmp(ifr->ifr_name, "fallback_gset") == 0) ||
               (strcmp(ifr->ifr_name, "speed_unknown") == 0) ||
               (strcmp(ifr->ifr_name, "speed_10") == 0) ||
               (strcmp(ifr->ifr_name, "speed_2500") == 0)) {
                // If the name is fallback_gset linksettings should fail so we can fall back on the legacy functions
                rv = -1;
            } else {
                // ETHTOOL_GLINKSETTINGS for a GET change the name to eth0 and perform an actual call
                strncpy(ifr->ifr_name, "eth0", IF_NAMESIZE);
                ifr->ifr_name[IF_NAMESIZE - 1] = '\0';
                rv = __real_ioctl(d, request, ifr);
            }
        } else {
            struct ethtool_cmd* ecmd2;
            ecmd2 = (struct ethtool_cmd*) ifr->ifr_data;
            if(ecmd2->cmd == ETHTOOL_GSET) {
                ecmd2->speed = 10000;
                ecmd2->duplex = DUPLEX_FULL;
                if(strcmp(ifr->ifr_name, "speed_unknown") == 0) {
                    ecmd2->supported = 0;
                } else if(strcmp(ifr->ifr_name, "speed_10") == 0) {
                    ecmd2->supported = SUPPORTED_10baseT_Half;
                } else if(strcmp(ifr->ifr_name, "speed_2500") == 0) {
                    ecmd2->supported = SUPPORTED_2500baseX_Full;
                }
                rv = 0;
            } else if(ecmd2->cmd == ETHTOOL_SSET) {
                if(strcmp(ifr->ifr_name, "speed_unknown") == 0) {
                    assert_string_equal("", "Should have failed, not allow to reach this");
                } else if(strcmp(ifr->ifr_name, "speed_10") == 0) {
                    assert_int_equal(ecmd2->speed, 10);
                    assert_int_equal(ecmd2->duplex, DUPLEX_FULL);
                } else if(strcmp(ifr->ifr_name, "speed_2500") == 0) {
                    assert_int_equal(ecmd2->speed, 2500);
                    assert_int_equal(ecmd2->duplex, DUPLEX_FULL);
                } else {
                    assert_int_equal(ecmd2->speed, 1000);
                    assert_int_equal(ecmd2->duplex, DUPLEX_HALF);
                }
                rv = 0;
            }
        }
    } else if(strcmp(ifr->ifr_name, "disable") == 0) {
        if(request == SIOCGIFFLAGS) {
            ifr->ifr_flags = (short int) 0xffffffffffffffff;
        } else if(request == SIOCSIFFLAGS) {
            // Check if the IFF_UP flag is not set (disabled)
            assert_int_equal(ifr->ifr_flags, 0xffffffffffffffff & ~IFF_UP);
        }
        rv = 0;
    } else if(strcmp(ifr->ifr_name, "enable") == 0) {
        if(request == SIOCGIFFLAGS) {
            ifr->ifr_flags = (short int) 0;
        } else if(request == SIOCSIFFLAGS) {
            // Check if the IFF_UP flag is set (enabled)
            assert_int_equal(ifr->ifr_flags, IFF_UP);
        }
        rv = 0;
    } else if(strcmp(ifr->ifr_name, "noarp") == 0) {
        if(request == SIOCGIFFLAGS) {
            ifr->ifr_flags = (short int) 0;
        } else if(request == SIOCSIFFLAGS) {
            // Check if the IFF_NOARP flag is set (enabled)
            assert_int_equal(ifr->ifr_flags, IFF_NOARP);
        }
        rv = 0;
    } else if(strcmp(ifr->ifr_name, "arp") == 0) {
        if(request == SIOCGIFFLAGS) {
            ifr->ifr_flags = (short int) 0;
        } else if(request == SIOCSIFFLAGS) {
            // Check if the IFF_NOARP flag is not set (disabled)
            assert_int_equal(ifr->ifr_flags, 0);
        }
        rv = 0;
    } else if(strcmp(ifr->ifr_name, "fail_during_set") == 0) {
        if(request == SIOCSIFHWADDR) {
            // Fail the set
            rv = -1;
        } else if(request == SIOCGIFHWADDR) {
            // Make the get successful
            ifr->ifr_hwaddr.sa_family = ARPHRD_ETHER;
            rv = 0;
        }
    } else if(strcmp(ifr->ifr_name, "mac_set_intf") == 0) {
        if(request == SIOCSIFHWADDR) {
            assert_string_equal("", "Write did not notice that we are trying to set the same mac address");
        } else if(request == SIOCGIFHWADDR) {
            // A get will be done before a set, return the same mac as we are trying to set in test_mac_address_already_set
            const unsigned char mac[] = {0x00, 0xCA, 0xFE, 0xC0, 0xFF, 0xEE};
            memcpy(ifr->ifr_hwaddr.sa_data, mac, ETH_ALEN);
            ifr->ifr_hwaddr.sa_family = ARPHRD_ETHER;
            rv = 0;
        }
    } else if(strcmp(ifr->ifr_name, "memory_intf") == 0) {
        if(request == SIOCSIFHWADDR) {
            // Allocate memory for the MAC address
            mac_address = malloc(ETH_ALEN);

            // Store written MAC address bytes
            memcpy(mac_address, ifr->ifr_hwaddr.sa_data, ETH_ALEN);
        } else if(request == SIOCGIFHWADDR) {
            // Copy the stored MAC address to the provided buffer
            if(mac_address != NULL) {
                memcpy(ifr->ifr_hwaddr.sa_data, mac_address, ETH_ALEN);
                free(mac_address);
            } else {
                // A get will be done before a set to make sure the MAC is different, set the initial value
                const unsigned char mac[] = {0xCA, 0xFE, 0xC0, 0xFF, 0xEE, 0x00};
                memcpy(ifr->ifr_hwaddr.sa_data, mac, ETH_ALEN);
                ifr->ifr_hwaddr.sa_family = ARPHRD_ETHER;
            }
        }
        rv = 0;
    } else {
        rv = __real_ioctl(d, request, ifr);
    }

    return rv;
}

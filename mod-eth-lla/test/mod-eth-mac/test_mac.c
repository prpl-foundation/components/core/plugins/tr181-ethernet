/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <regex.h>
#include <linux/sockios.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "../../include_priv/mod_eth_lla.h"
#include "test_mac.h"

#define MOD_NAME "target_module"
#define MAC_PATTERN "^([0-9a-fA-F]{2}:){5}([0-9a-fA-F]{2})$"

static int regex_comp(const char* pattern, const char* input) {
    regex_t regex;
    int rv;

    rv = regcomp(&regex, pattern, REG_EXTENDED | REG_NOSUB);
    when_failed(rv, exit);

    rv = regexec(&regex, input, 0, NULL, 0);

exit:
    regfree(&regex);
    return rv;
}

int test_setup(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, MOD_NAME, "../modules_under_test/target_module.so"), 0);
    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(amxm_close_all(), 0);
    return 0;
}

void test_mac_address_missing_parameters(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    // Get with the intf_name parameter missing
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-get", &var, &ret), -1);

    // Set with both the intf_name and mac_address missing
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-set", &var, &ret), -1);

    // Set with only one parameter missing
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "memory_intf");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-set", &var, &ret), -1);
    amxc_var_clean(&var);
    amxc_var_clean(&ret);

    amxc_var_init(&var);
    amxc_var_init(&ret);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "mac_address", "00:00:FA:CA:DE:00");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-set", &var, &ret), -1);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_mac_address_invalid_parameters(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    // No interface with eth99 should exist
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "eth99");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-get", &var, &ret), -1);

    // No interface with eth99 should exist, set will also fail
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-set", &var, &ret), -1);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_mac_address_set_fail(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    // This will get true the get fase but not the set
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "fail_during_set");
    amxc_var_add_key(cstring_t, &var, "mac_address", "00:00:FA:CA:DE:00");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-set", &var, &ret), -1);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_mac_address_already_set(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    // Try to set a mac address on a mac_already_set_intf. __wrap_ioctl will store this mac for when we read the mac again.
    // Interface is hard coded in the __wrap_ioctl implementation
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "mac_set_intf");
    amxc_var_add_key(cstring_t, &var, "mac_address", "00:CA:FE:C0:FF:EE");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-set", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
    amxc_var_init(&var);
    amxc_var_init(&ret);

    // Try to read the mac address we just set
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "mac_set_intf");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-get", &var, &ret), 0);

    assert_non_null(GET_ARG(&ret, "mac_address"));
    // MAC should be returned as a lowercase value
    assert_string_equal(GET_CHAR(&ret, "mac_address"), "00:ca:fe:c0:ff:ee");

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_mac_address_not_ethernet(UNUSED void** state) {
    int rv = -1;
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    // lo is not an ethernet interface, we should be abel to read but not set the interface
    // lo doesn't need mocking, should exist in test setup
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "lo");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-get", &var, &ret), 0);

    assert_non_null(GET_ARG(&ret, "mac_address"));
    rv = regex_comp(MAC_PATTERN, GET_CHAR(&ret, "mac_address"));
    assert_int_equal(rv, 0);

    amxc_var_add_key(cstring_t, &var, "mac_address", "00:00:FA:CA:DE:00");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-set", &var, &ret), -1);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_mac_address_set_get(UNUSED void** state) {
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    // Try to set a mac address on a memory_intf. __wrap_ioctl will store this mac for when we read the mac again.
    // Interface is hard coded in the __wrap_ioctl implementation
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "memory_intf");
    amxc_var_add_key(cstring_t, &var, "mac_address", "00:00:FA:CA:DE:00");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-set", &var, &ret), 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
    amxc_var_init(&var);
    amxc_var_init(&ret);

    // Try to read the mac address we just set
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "memory_intf");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-get", &var, &ret), 0);

    assert_non_null(GET_ARG(&ret, "mac_address"));
    // MAC should be returned as a lowercase value
    assert_string_equal(GET_CHAR(&ret, "mac_address"), "00:00:fa:ca:de:00");

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

void test_mac_address_get(UNUSED void** state) {
    int rv = -1;
    amxc_var_t var;
    amxc_var_t ret;

    amxc_var_init(&var);
    amxc_var_init(&ret);

    // Read the mac for eth0 (should exist in test setup), should return a valid mac address
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &var, "intf_name", "eth0");
    assert_int_equal(amxm_execute_function(MOD_NAME, MOD_ETH_LLA_CTRL, "mac-address-get", &var, &ret), 0);

    assert_non_null(GET_ARG(&ret, "mac_address"));
    rv = regex_comp(MAC_PATTERN, GET_CHAR(&ret, "mac_address"));
    assert_int_equal(rv, 0);

    amxc_var_clean(&var);
    amxc_var_clean(&ret);
}

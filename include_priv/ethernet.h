/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__ETHERNET_MANAGER_H__)
#define __ETHERNET_MANAGER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxb/amxb.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>

#include <amxm/amxm.h>

#include <amxs/amxs.h>

#include <netmodel/client.h>

#define str_empty(x) ((x == NULL) || (*x == '\0'))

// Defines for manually used status' in this code
#define STATUS_DOWN "down"
#define STATUS_ERROR "Error"
// Status used for the LED object status
#define STATUS_ENABLED "Enabled"
#define STATUS_DISABLED "Disabled"
#define STATUS_NOT_PRESENT "NotPresent"
#define STATUS_UNKNOWN "Unknown"

// Defines for status used in RMONSTATES
#define RMONSTATS_STATUS_DISABLED "Disabled"
#define RMONSTATS_STATUS_ENABLED "Enabled"
#define RMONSTATS_STATUS_ERR_MISCONFIGURED "Error_Misconfigured"
#define RMONSTATS_STATUS_ERR "Error"

// Name of Control Parameter in the datamodel
#define VLAN_CTRL "VLANController"
#define ETH_LLA_CTRL "DefaultController"
#define ETH_LED_CTRL "LEDController"
#define ETH_RMONSTATS_CTRL "RMONStatsController"

// Name of the module namespace that you want to use to execute the functions
#define MOD_VLAN_CTRL "vlan-ctrl"
#define MOD_ETH_LLA_CTRL "eth-lla-ctrl"
#define MOD_ETH_LED_CTRL "eth-led-ctrl"
#define MOD_ETH_RMONSTATS_CTRL "eth-rmonstats-ctrl"

// Define the name of the module to be used when the controllers fail to load
#define DUMMY_VLAN_MOD_NAME "mod-vlan-dummy"
#define DUMMY_LED_MOD_NAME "mod-led-dummy"

typedef enum rmonstats_status {
    RMONSTATS_DISABLED,
    RMONSTATS_ENABLED,
    RMONSTATS_ERR_MISCONFIGURED,
    RMONSTATS_ERR,
    RMONSTATS_NUM_STATUS
} rmonstats_status_t;

typedef enum _ethernet_instance_type {
    ETHERNET_INTERFACE,
    ETHERNET_LINK,
    ETHERNET_VLANTERMINATION,
} ethernet_instance_type_t;

typedef struct _ethernet_app {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxb_bus_ctx_t* netdev_ctx;
    bool vlan_mods_are_loaded;
    bool led_mods_are_loaded;
} ethernet_app_t;

typedef struct _ethernet_info {
    amxd_object_t* object;                             // pointer to object this structure belongs to
    ethernet_instance_type_t type;                     // type of ethernet instance this info struct belongs to
    char* netdev_intf;                                 // netdev interface name
    amxb_subscription_t* netdev_status_subscription;   // subscription to be informed of status updates in NetDev
    amxb_subscription_t* netdev_new_link_subscription; // subscription to be informed of new matching links, added in NetDev
    amxb_subscription_t* netdev_mac_subscription;      // subscription to be informed of LLaddress updates in NetDev
    uint32_t last_change;                              // time definition for time of last status change
    netmodel_query_t* query;                           // netmodel query for the ethernet link netdev name, used for VLANs
    char* link_name;                                   // Netdev name of the ethernet link, used for VLANs
    amxd_object_t* led_object;                         // pointer to led object
    amxs_sync_ctx_t* sync_to_switch;                   // pointer to data model sync context to sync to switch
    amxs_sync_ctx_t* sync_from_switch;                 // pointer to data model sync context to sync from switch
    amxs_sync_ctx_t* sync_from_cpu_port;               // pointer to data model sync context to sync from cpu port interface
} ethernet_info_t;

typedef struct rmonstate_states_conv {
    const char* str_status;
    rmonstats_status_t enum_status;
} rmonstate_states_conv_t;

typedef struct _ethernet_rmonstats_info {
    bool enable;
    rmonstats_status_t status;

    //The statistics parameters
    char* interface;

    amxd_object_t* object;
} ethernet_rmonstats_info_t;

amxd_dm_t* PRIVATE ethernet_get_dm(void);
amxb_bus_ctx_t* PRIVATE ethernet_get_netdev_ctx(void);
amxo_parser_t* PRIVATE ethernet_get_parser(void);
bool PRIVATE ethernet_vlan_mods_are_loaded(void);
bool PRIVATE ethernet_led_mods_are_loaded(void);
int _ethernet_manager_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);

#ifdef __cplusplus
}
#endif

#endif // __ETHERNET_MANAGER_H__

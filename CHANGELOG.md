# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.24.0 - 2024-12-19(13:37:21 +0000)

### Other

- LAN Eth devices with empty MACAddress

## Release v1.23.0 - 2024-12-13(14:06:35 +0000)

### Other

- Additional ETH0 instance in Device.Ethernet.Interface.

## Release v1.22.11 - 2024-12-11(12:59:33 +0000)

### Other

- [ethernet-manager] Update list of default controllers

## Release v1.22.10 - 2024-12-06(15:03:55 +0000)

### Other

- [switch-manager] add odl templates

## Release v1.22.9 - 2024-11-25(13:33:42 +0000)

### Other

- [odl-generator] add support for cellular-manager
- [ethernet-manager] Sync Ethernet data model with Switch data model

## Release v1.22.8 - 2024-10-21(16:27:00 +0000)

### Other

- - [LEDs] Rear leds not turned off by Ethernet API after reboot

## Release v1.22.7 - 2024-10-21(16:19:35 +0000)

### Other

- - [odl][tr181] Remove failing line in unit tests

## Release v1.22.6 - 2024-10-11(09:20:12 +0000)

### Other

- - [Terminating dot][tr181] fix Ethernet Manager

## Release v1.22.5 - 2024-09-26(07:49:36 +0000)

### Other

- - [Terminating dot][tr181] fix Ethernet Manager

## Release v1.22.4 - 2024-09-11(16:02:30 +0000)

### Other

- - "[Network Settings]: Access point edited names not restored after firmware upgrade"

## Release v1.22.3 - 2024-09-10(07:08:28 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v1.22.2 - 2024-09-02(15:55:15 +0000)

### Other

- - High Level API to support MTU, and MTU Type

## Release v1.22.1 - 2024-09-02(07:59:17 +0000)

## Release v1.22.0 - 2024-08-23(08:30:55 +0000)

### New

- Problem: No support for Ethernet RMONStats (implementation)

## Release v1.21.11 - 2024-07-25(13:57:55 +0000)

### Other

- Typo in RMONSTATS.Status parameter

## Release v1.21.10 - 2024-07-23(07:36:05 +0000)

### Fixes

- Better shutdown script

## Release v1.21.9 - 2024-07-12(13:43:16 +0000)

### Other

- - [LEDs] Dimming of rear LEDs

## Release v1.21.8 - 2024-06-21(19:11:34 +0000)

### Other

- - WAN link not enabled by default in the datamodel

## Release v1.21.7 - 2024-06-21(08:43:10 +0000)

### Other

- - [tr181-ethernet] add RMONStats DM

## Release v1.21.6 - 2024-05-17(09:12:19 +0000)

### Fixes

- - VLAN not created after reset or mode switch

## Release v1.21.5 - 2024-05-02(08:11:17 +0000)

### Other

- do not generate wan Ethernet.Link.x. instances when no...

## Release v1.21.4 - 2024-04-24(11:13:55 +0000)

### Fixes

- rework default templates to allow multiple wan interfaces

## Release v1.21.3 - 2024-04-10(10:08:22 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.21.2 - 2024-03-25(08:01:46 +0000)

### Fixes

- - Make SupportedControllers persistent

## Release v1.21.1 - 2024-03-25(07:22:03 +0000)

### Other

- Remove space in odl file

## Release v1.21.0 - 2024-03-23(13:12:19 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v1.20.3 - 2024-03-12(15:02:46 +0000)

### Changes

- Configure lla module per Ethernet.Interface instance

## Release v1.20.2 - 2024-03-07(19:18:45 +0000)

### Changes

- [TR181 Ethernet] Make VLANTerminations upgrade persistent

## Release v1.20.1 - 2024-02-08(09:14:34 +0000)

### Fixes

- [TR181 Ethernet] Load default bridge MAC addresses correctly

## Release v1.20.0 - 2024-02-06(14:00:35 +0000)

### New

- Dynamically add bridges based on networklayout.json

## Release v1.19.0 - 2024-02-05(16:24:08 +0000)

### New

- Add ability to manually configure MTU

## Release v1.18.0 - 2024-02-01(10:54:39 +0000)

### New

- Get description field from networklayout.json

## Release v1.17.1 - 2024-01-30(14:01:01 +0000)

### Fixes

- Build issue, undeclared identifier

## Release v1.17.0 - 2024-01-25(14:26:11 +0000)

### New

- Add a Description field to allow extra information

## Release v1.16.0 - 2024-01-23(20:30:20 +0000)

### New

- Add the support of specific vendor module

## Release v1.15.2 - 2024-01-11(12:11:39 +0000)

### Fixes

- Wrong mac defaults for ap config

## Release v1.15.1 - 2024-01-04(08:48:02 +0000)

### Fixes

- MaxBitrate and DuplexMode not upgrade persistent

## Release v1.15.0 - 2023-12-23(13:50:51 +0000)

### New

- If the MAC address changes, DHCPv6 client must be retriggered

## Release v1.14.0 - 2023-12-13(16:29:10 +0000)

### New

- Make Ethernet interfaces persistent

## Release v1.13.4 - 2023-12-12(11:45:43 +0000)

### Other

- create new entry if moca  is upstream/wan interface

## Release v1.13.3 - 2023-12-08(14:17:30 +0000)

### Fixes

- Parameters for Ethernet Ports not setting (WAN)

## Release v1.13.2 - 2023-11-24(16:05:56 +0000)

### Fixes

- Constant carrier changes on the wan interface

## Release v1.13.1 - 2023-11-24(14:40:02 +0000)

### Fixes

- Change GLINKSETTINGS log level from error to info, so it's not constantly printed

## Release v1.13.0 - 2023-10-28(07:37:45 +0000)

### New

- Configure VLANPriority when creating VLANs

## Release v1.12.4 - 2023-10-28(07:09:36 +0000)

### Fixes

- Changing the MAC in linux should not update the MAC in the DM

## Release v1.12.3 - 2023-10-26(13:18:21 +0000)

### Fixes

- Ethernet manager is not applying mac addresses correctly

## Release v1.12.2 - 2023-10-20(09:30:35 +0000)

### Changes

- Add unique MACAddress parameter to the different instances

## Release v1.12.1 - 2023-10-13(14:13:46 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v1.12.0 - 2023-09-25(12:41:49 +0000)

### New

- [amxrt][no-root-user][capability drop] tr181-ethernet plugin must be adapted to run as non-root and lmited capabilities

## Release v1.11.0 - 2023-09-19(09:49:53 +0000)

### New

- Enable loopback interface at boot

## Release v1.10.6 - 2023-09-09(06:27:23 +0000)

### Other

- Add support for ioctl ETHTOOL_xLINKSETTINGS

## Release v1.10.5 - 2023-08-10(12:55:29 +0000)

### Fixes

- [TR181-Ethernet] MAC address are not updated if set externally by the system

## Release v1.10.4 - 2023-08-01(11:50:02 +0000)

### Fixes

- The DuplexMode and MaxBitRate doesn't change the bitrate and duplex mode

## Release v1.10.3 - 2023-07-19(12:35:03 +0000)

### Fixes

- Should not be able to have more than one upstream interface configured

## Release v1.10.2 - 2023-06-08(16:29:30 +0000)

### Fixes

- Add PON interface(s) to ucode files

## Release v1.10.1 - 2023-05-24(06:59:28 +0000)

### Other

- - [HTTPManager][WebUI] Create plugin's ACLs permissions

## Release v1.10.0 - 2023-05-18(05:39:01 +0000)

### New

- Add support for GPON as physicalType in wan-manager

## Release v1.9.11 - 2023-04-21(10:20:08 +0000)

### Fixes

- Handle a dot in the NetDev name

## Release v1.9.10 - 2023-04-12(09:42:34 +0000)

### Fixes

- Sometimes after a reboot the bridge is down

## Release v1.9.9 - 2023-04-05(16:32:29 +0000)

### Fixes

-  tr181-ethernet *.LastChanges are wrong in datamodel

## Release v1.9.8 - 2023-04-05(16:29:12 +0000)

### Fixes

- Sometimes after a reboot the bridge is down

## Release v1.9.7 - 2023-04-03(11:44:49 +0000)

### Other

- Add missing MACAddress parameter in ucode files

## Release v1.9.6 - 2023-03-22(08:33:01 +0000)

### Fixes

- Sometimes after a reboot the bridge is down

## Release v1.9.5 - 2023-03-17(18:34:44 +0000)

### Other

- [baf] Correct typo in config option

## Release v1.9.4 - 2023-03-16(15:21:54 +0000)

### Other

- Remove dependency on odl-generator

## Release v1.9.3 - 2023-03-16(12:57:47 +0000)

### Other

- Add AP config files

## Release v1.9.2 - 2023-03-09(12:02:57 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v1.9.1 - 2023-03-02(12:58:08 +0000)

### Other

- Move odl templates to components

## Release v1.9.0 - 2023-02-02(15:00:09 +0000)

### New

- Issue:   HOP-598  [NetDev][ethernet manager] Extend netdev with support of ethtool status parameters

## Release v1.8.0 - 2023-01-05(11:54:06 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v1.7.1 - 2022-12-09(09:20:21 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v1.7.0 - 2022-09-27(12:57:32 +0000)

### New

- Add default mac addresses for bridges

## Release v1.6.8 - 2022-09-23(07:24:28 +0000)

### Fixes

- [lla config] No connection to the board from lan side after a reset hard

## Release v1.6.7 - 2022-09-01(06:26:14 +0000)

### Fixes

- the interface disable/enable changes more than IFF_UP bit

## Release v1.6.6 - 2022-06-23(17:22:01 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v1.6.5 - 2022-04-13(11:50:46 +0000)

### Changes

- Vlan Interfaces are not up after creation

## Release v1.6.4 - 2022-04-08(08:51:33 +0000)

### Changes

- Enable basic lan connectivity on LLA config

## Release v1.6.3 - 2022-03-24(11:00:15 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.6.2 - 2022-03-17(12:45:01 +0000)

### Fixes

- Change NetDev subscription from Name to Alias based

## Release v1.6.1 - 2022-03-17(10:43:53 +0000)

### Changes

- Let NetDev report the lo interface status as up when it is unknown

## Release v1.6.0 - 2022-03-03(14:30:13 +0000)

### New

- [CONFIG] add lcm interface to config

## Release v1.5.6 - 2022-02-27(07:53:20 +0000)

### Other

- [TR181-ethernet] Apply proper default values

## Release v1.5.5 - 2022-02-25(11:09:50 +0000)

### Other

- Enable core dumps by default

## Release v1.5.4 - 2022-02-17(07:37:59 +0000)

### Fixes

- Loopback status unkown when link is up

## Release v1.5.3 - 2022-01-11(08:31:07 +0000)

### Changes

- create mib_ethernet

## Release v1.5.2 - 2021-11-29(09:00:24 +0000)

### Fixes

- Apply correct default configuration for a guest network

## Release v1.5.1 - 2021-11-23(08:18:31 +0000)

### Other

- [ACL] The EthernetManager must have default ACLS files configured

## Release v1.5.0 - 2021-11-15(16:18:27 +0000)

### New

- Use mod_netmodel to populate NetModel with the Ethernet instances

## Release v1.4.0 - 2021-10-28(07:18:29 +0000)

### New

- Add mod_vlan_uci as an optional VLANController

## Release v1.3.0 - 2021-10-19(06:32:33 +0000)

### New

- Fill in Device.Ethernet.Link.{i}. part

## Release v1.2.0 - 2021-10-13(12:54:06 +0000)

### New

- Create support for VLANTerminations

## Release v1.1.1 - 2021-10-11(08:04:07 +0000)

### Fixes

- TR181 Ethernet set start up order to 21

## Release v1.1.0 - 2021-08-26(17:01:28 +0000)

### New

- Added populate section to reflect the development board status

## Release v1.0.0 - 2021-06-29(14:46:05 +0200)

### New

- First delivery with datamodel

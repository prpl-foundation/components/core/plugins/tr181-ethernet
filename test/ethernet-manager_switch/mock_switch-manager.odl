%define {
    %persistent object Switch {
        /**
        * Indicates which switch module must be loaded. Each switch will have a unique identifier.
        * Module to be loaded per specific hardware.
        *
        * @version 1.0
        */
        %persistent %read-only string Controller = "";

        /**
        * Multi-instance object which represents the per port information.
        * The number of ports is fixed and based on the hardware capabilities.
        *
        * @version 1.0
        */
        %persistent object Port[] {
            /**
            * Unique key representing the port.
            * An alternate name used to identify the interface, which is assigned an initial value
            * by the CPE, but can later be chosen by the Controller.
            *
            * @version 1.0
            */
            %persistent %unique %key string Alias;

            /**
            * Port ID, specific to the switch implementation.
            *
            * @version 1.0
            */
            %persistent %read-only int32 PortID;

            /**
            * Enable or disable the port.
            *
            * @version 1.0
            */
            %persistent bool Enable {
                on action read call dm_get_port_enable;
            }

            /**
            * The status of the port. Enumeration of:
            *   Disabled
            *   Enabled
            *   Error
            *
            * @version 1.0
            */
            %read-only string Status {
                default "Disabled";
                on action validate call check_enum 
                    ["Disabled", "Enabled", "Error"];
            }

            /**
            * Detailed port Status, depending on the switch capabilities.
            *
            * @version 1.0
            */
            %read-only string PortStatus {
                default "Down";
            }

            /**
            * The textual name used to identify the interface, which is chosen by the CPE.
            *
            * @version 1.0
            */
            %persistent string Name;

            /**
            * Free format field to describe the port in more detail.
            *
            * @version 1.0
            */
            %persistent string Description;

            /**
            * Lower layers of the switch port. Typically an empty string, since it is not expected
            * that a switch port has some lower layers defined.
            *
            * @version 1.0
            */
            %persistent string LowerLayers;

            /**
            * The accumulated time in seconds since the interface entered its current operational
            * state.
            *
            * @version 1.0
            */
            %read-only datetime LastChange;

            /**
            * Maximum bit rate in Mbps.
            *
            * @version 1.0
            */
            %persistent int32 MaxBitRate {
                on action read call dm_get_port_max_bitrate;
            }

            /**
            * Current bit rate in Mbps.
            *
            * @version 1.0
            */
            %persistent %read-only uint32 CurrentBitRate {
                on action read call dm_get_port_current_bitrate;
            }

            /**
            * The duplex mode of the connection.
            *
            * Enumeration of:
            *   Half
            *   Full
            *   Auto
            *
            * @version 1.0
            */
            %persistent string DuplexMode {
                on action read call dm_get_port_duplex_mode;
            }

            /**
            * Indicates whether this physical ethernet port supports Energy Efficient Ethernet.
            *
            * @version 1.0
            */
            %read-only bool EEECapability {
                on action read call dm_get_port_eee_capability;
            }

            /**
            * Whether Energy Efficient Ethernet support is currently enabled.
            *
            * @version 1.0
            */
            %persistent bool EEEEnable {
                on action read call dm_get_port_eee_enable;
            }

            /**
            * Indicates the active state of Energy Efficient Ethernet.
            *
            * The Disabled value indicates that EEEEnable is disabled.
            *
            * The Active value indicates that EEEEnable is enabled and that EEE support has been
            * negotiated with the link partner. In this state EEE will be used.
            *
            * The Inactive value indicates that EEEEnable is disabled, or that EEE support has not
            * been negotiated with the link partner, either because the link parter is not EEE
            * capable, or its support for EEE is disabled.
            *
            * The Unsupported value indicates that this physical interface does not support EEE,
            * in which case EEECapability will be false.
            * 
            * Enumeration of:
            * 
            *     Disabled
            *     Active
            *     Inactive
            *     Unsupported
            *
            * @version 1.0
            */
            %read-only string EEEStatus {
                default "Disabled";
            }

            /**
            * Displays the bridge table per port.
            *
            * @version 1.0
            */
            %read-only object BridgeTable {
                /**
                * An entry in the table.
                *
                * @version 1.0
                */
                %read-only object Entry[] {
                    /**
                    * MAC address format, colon separated hexadecimal values in capital letters.
                    */
                    %read-only string MACAddress;
                }
            }

            /**
            * Port statistics.
            *
            * @version 1.0
            */
            %read-only object Stats {
                on action read call dm_get_port_stats;
                uint64 BytesSent;
                uint64 BytesReceived;
                uint64 UnicastPacketsSent;
                uint64 UnicastPacketsReceived;
                uint64 MulticastPacketsSent;
                uint64 MulticastPacketsReceived;
                uint64 BroadcastPacketsSent;
                uint64 BroadcastPacketsReceived;
                uint64 DiscardPacketsSent;
                uint64 DiscardPacketsReceived;
                uint64 ErrorsSent;
                uint64 ErrorsReceived;
            }

            /**
            * RMON statistics.
            *
            * @version 1.0
            */
            %read-only object RMONStats {
                on action read call dm_get_port_rmon_stats;
                uint32 Bytes;
                uint32 Packets;
                uint32 DropEvents;
                uint32 BroadcastPackets;
                uint32 MulticastPackets;
                uint32 CRCErroredPackets;
                uint32 UndersizePackets;
                uint32 OversizePackets;
                uint32 Packets64Bytes;
                uint32 Packets65to127Bytes;
                uint32 Packets128to255Bytes;
                uint32 Packets256to511Bytes;
                uint32 Packets512to1023Bytes;
                uint32 Packets1024to1518Bytes;
            }

            /**
            * LED object describes the PHY Port LED.
            *
            * @version 1.0
            */
            %persistent object LED {

                /**
                * Indicates whether the phy port LED is turned on or off.
                *
                * @version 1.0
                */
                %persistent bool Enable = true;

                /**
                * The current operational state of the interface port LED. Enumeration of:
                * Enabled
                * Error
                * Disabled
                * NotPresent
                * Unknown
                *
                * @version 1.0
                */
                %read-only string Status = "Unknown" {
                    on action validate call check_enum ["Enabled", "Error", "Disabled", "NotPresent", "Unknown"];
                }
            }
        }
    }
}

%populate {
    object 'Switch' {
        parameter 'Controller' = "mod-switch-test";
        object 'Port' {
            instance add(0, 'cpe-Port-1', 'Alias' = "cpe-Port-1") {
                parameter PortID = 1;
            }
        }
    }
}

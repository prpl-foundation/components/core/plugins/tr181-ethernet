/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>
#include <linux/sockios.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxb/amxb_register.h>

#include "../common/common_functions.h"

#include "ethernet.h"
#include "ethernet_events.h"
#include "ethernet_actions.h"

#include "../common/mock.h"
#include "test_dm_events_actions.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

static const char* odl_config = "../common/mock.odl";
static const char* odl_nm = "../common/mock_netmodel.odl";
static const char* odl_defs = "../../odl/ethernet-manager_definition.odl";
static const char* odl_mock_netdev = "mock_netdev_dm.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_netdev, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_nm, root_obj), 0);

    assert_int_equal(_ethernet_manager_main(AMXO_START, &dm, &parser), 0);

    handle_events();

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    _ethernet_manager_main(AMXO_STOP, &dm, &parser);

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    handle_events();

    test_unregister_dummy_be();

    return 0;
}

void test_link_default_mac(UNUSED void** state) {
    amxd_object_t* link_obj = amxd_dm_findf(&dm, "Ethernet.Link.test_link.");
    assert_non_null(link_obj);

    assert_string_equal(GET_CHAR(amxd_object_get_param_value(link_obj, "MACAddress"), NULL), "01:23:45:67:89:AB");
}

void test_allowed_controllers(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "DefaultController", "A not allowed value");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "DefaultController", "");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "DefaultController", "mod-eth-dummy");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_allowed_vlan_controllers(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "VLANController", "A not allowed value");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "VLANController", "");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "VLANController", "mod-vlan-dummy");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_interface_create(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Interface.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_add_inst(&trans, 0, "ETH5");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Name", "eth5");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

}

void test_interface_enable(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Interface.ETH5.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_interface_upstream(UNUSED void** state) {
    amxd_trans_t trans;

    // remove the default upstream value
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Interface.ETH0.");
    amxd_trans_set_value(bool, &trans, "Upstream", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Interface.ETH0.");
    amxd_trans_set_value(bool, &trans, "Upstream", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Interface.ETH1.");
    amxd_trans_set_value(bool, &trans, "Upstream", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_interface_led(UNUSED void** state) {
    amxd_object_t* led_obj = NULL;
    amxd_trans_t trans;
    char* tmp_str = NULL;

    // Verify the SupportedLEDControllers is defined
    led_obj = amxd_dm_findf(&dm, "Ethernet");
    tmp_str = amxd_object_get_value(cstring_t, led_obj, "SupportedLEDControllers", NULL);
    assert_string_equal(tmp_str, "mod-led-dummy");
    free(tmp_str);

    // Verify the LED object is present
    led_obj = amxd_dm_findf(&dm, "Ethernet.Interface.ETH1.TEST_LED");
    assert_non_null(led_obj);

    // Verify the LED.Status is not present for interface without LEDController
    tmp_str = amxd_object_get_value(cstring_t, led_obj, "Status", NULL);
    assert_string_equal(tmp_str, "NotPresent");
    free(tmp_str);

    // Verify the LEDController is defined
    led_obj = amxd_dm_findf(&dm, "Ethernet.Interface.ETH0");
    tmp_str = amxd_object_get_value(cstring_t, led_obj, "LEDController", NULL);
    assert_string_equal(tmp_str, "mod-led-dummy");
    free(tmp_str);

    // Verify the LED.Status is correctly set
    led_obj = amxd_dm_findf(&dm, "Ethernet.Interface.ETH0.TEST_LED");
    tmp_str = amxd_object_get_value(cstring_t, led_obj, "Status", NULL);
    assert_true(amxd_object_get_value(bool, led_obj, "Enable", NULL));
    assert_string_equal(tmp_str, "Enabled");
    free(tmp_str);

    // Turn off the LED
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Interface.ETH0.TEST_LED.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
    assert_false(amxd_object_get_value(bool, led_obj, "Enable", NULL));

    // Verify the status changed
    tmp_str = amxd_object_get_value(cstring_t, led_obj, "Status", NULL);
    assert_string_equal(tmp_str, "Disabled");
    free(tmp_str);
}

void test_link_create(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Link.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_add_inst(&trans, 0, "ETH0");
    amxd_trans_set_value(cstring_t, &trans, "Name", "eth0");
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Interface.1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "LAN");
    amxd_trans_set_value(cstring_t, &trans, "Name", "br-lan");
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Bridging.Bridge.1.Port.1");
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", "12:34:56:78:9A:BC");
    amxd_trans_set_value(bool, &trans, "Enable", true);

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_vlanterm_create_wrong_same_name(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.");
    amxd_trans_add_inst(&trans, 0, "VLAN10");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.1");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 3);

    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "VLAN10_BIS");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.2");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 3);

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_create_arg", &data, &ret);
    const char* port_name = GETP_CHAR(&ret, "port_name");
    assert_null(port_name);

    const char* vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_null(vlanport_name);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_vlanterm_create_wrong_same_id(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.");
    amxd_trans_add_inst(&trans, 0, "VLAN10");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.1");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 3);

    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "VLAN10_BIS");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10_bis");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.1");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 3);

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_create_arg", &data, &ret);
    const char* port_name = GETP_CHAR(&ret, "port_name");
    assert_null(port_name);

    const char* vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_null(vlanport_name);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_vlanterm_create_same_id_diff_name_diff_ll(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.");
    amxd_trans_add_inst(&trans, 0, "VLAN10");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.1");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 3);

    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "VLAN10_BIS");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10_bis");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.2");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 3);

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.");
    amxd_trans_del_inst(&trans, 0, "VLAN10_BIS");

    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.");
    amxd_trans_del_inst(&trans, 0, "VLAN10");

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    amxd_trans_clean(&trans);
    handle_events();

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_create_arg", &data, &ret);
    const char* port_name = GETP_CHAR(&ret, "port_name");
    assert_null(port_name);

    const char* vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_null(vlanport_name);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_vlanterm_create(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.");
    amxd_trans_add_inst(&trans, 0, "VLAN10");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.1");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 3);

    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "VLAN10_BIS");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.1");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 3);

    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "VLAN20");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 20);
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 6);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan20");
    set_query_getResult_value("eth0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_create_arg", &data, &ret);
    const char* port_name = GETP_CHAR(&ret, "port_name");
    assert_non_null(port_name);
    assert_string_equal(port_name, "eth0");
    const char* vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_non_null(vlanport_name);
    assert_string_equal(vlanport_name, "vlan10");
    assert_int_equal(GETP_INT32(&ret, "vlanid"), 10);
    assert_int_equal(GETP_INT32(&ret, "egress_priority"), 3);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_vlanterm_enable(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.VLAN10");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_destroy_arg", &data, &ret);
    const char* vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_non_null(vlanport_name);
    assert_string_equal(vlanport_name, "vlan10");

    amxc_var_clean(&data);
    amxc_var_clean(&ret);

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.VLAN20");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    set_query_getResult_value("br-lan");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_create_arg", &data, &ret);
    const char* port_name = GETP_CHAR(&ret, "port_name");
    assert_non_null(port_name);
    assert_string_equal(port_name, "br-lan");
    vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_non_null(vlanport_name);
    assert_string_equal(vlanport_name, "vlan20");
    assert_int_equal(GETP_INT32(&ret, "vlanid"), 20);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_vlanterm_llayers_change(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* vlanterm_obj = NULL;
    char* llayers = NULL;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_init(&data);
    amxc_var_init(&ret);

    vlanterm_obj = amxd_dm_findf(&dm, "Ethernet.VLANTermination.VLAN20");
    llayers = amxd_object_get_value(cstring_t, vlanterm_obj, "LowerLayers", NULL);
    assert_string_not_equal(llayers, "Device.Ethernet.Link.1.");
    free(llayers);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.VLAN20");
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.1");
    set_query_getResult_value("eth0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_destroy_arg", &data, &ret);
    const char* vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_non_null(vlanport_name);
    assert_string_equal(vlanport_name, "vlan20");

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_create_arg", &data, &ret);
    const char* port_name = GETP_CHAR(&ret, "port_name");
    assert_non_null(port_name);
    assert_string_equal(port_name, "eth0");
    vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_non_null(vlanport_name);
    assert_string_equal(vlanport_name, "vlan20");
    assert_int_equal(GETP_INT32(&ret, "vlanid"), 20);
    assert_int_equal(GETP_INT32(&ret, "egress_priority"), 6);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_vlanterm_vlanid_change(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* vlanterm_obj = NULL;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_init(&data);
    amxc_var_init(&ret);

    vlanterm_obj = amxd_dm_findf(&dm, "Ethernet.VLANTermination.VLAN20");
    assert_int_not_equal(amxd_object_get_value(uint32_t, vlanterm_obj, "VLANID", NULL), 10);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.VLAN20");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    set_query_getResult_value("eth0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_destroy_arg", &data, &ret);
    const char* vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_non_null(vlanport_name);
    assert_string_equal(vlanport_name, "vlan20");

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_create_arg", &data, &ret);
    const char* port_name = GETP_CHAR(&ret, "port_name");
    assert_non_null(port_name);
    assert_string_equal(port_name, "eth0");
    vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_non_null(vlanport_name);
    assert_string_equal(vlanport_name, "vlan20");
    assert_int_equal(GETP_INT32(&ret, "vlanid"), 10);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_vlanterm_vlanprio_change(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* vlanterm_obj = NULL;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_init(&data);
    amxc_var_init(&ret);

    vlanterm_obj = amxd_dm_findf(&dm, "Ethernet.VLANTermination.VLAN20");
    assert_int_not_equal(amxd_object_get_value(uint32_t, vlanterm_obj, "VLANPriority", NULL), 1);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.VLAN20");
    amxd_trans_set_value(uint32_t, &trans, "VLANPriority", 1);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_prio_arg", &data, &ret);
    assert_int_equal(GETP_INT32(&ret, "egress_priority"), 1);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_link_delete(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* vlanterm_obj = NULL;
    amxd_object_t* vlanterm_obj2 = NULL;
    char* vlanterm_status = NULL;
    char* llayers = NULL;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_init(&data);
    amxc_var_init(&ret);

    vlanterm_obj = amxd_dm_findf(&dm, "Ethernet.VLANTermination.VLAN10");
    assert_non_null(vlanterm_obj);
    assert_false(amxd_object_get_value(bool, vlanterm_obj, "Enable", NULL));
    free(vlanterm_status);

    vlanterm_obj2 = amxd_dm_findf(&dm, "Ethernet.VLANTermination.VLAN20");
    assert_non_null(vlanterm_obj2);
    assert_true(amxd_object_get_value(bool, vlanterm_obj2, "Enable", NULL));
    vlanterm_status = amxd_object_get_value(cstring_t, vlanterm_obj2, "Status", NULL);
    assert_string_not_equal(vlanterm_status, "Down");
    free(vlanterm_status);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Link.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(amxd_object_get_value(bool, vlanterm_obj, "Enable", NULL), 0);
    llayers = amxd_object_get_value(cstring_t, vlanterm_obj, "LowerLayers", NULL);
    assert_string_equal(llayers, "");
    vlanterm_status = amxd_object_get_value(cstring_t, vlanterm_obj, "Status", NULL);
    assert_string_equal(vlanterm_status, "Down");
    free(llayers);
    free(vlanterm_status);

    assert_int_equal(amxd_object_get_value(bool, vlanterm_obj2, "Enable", NULL), 0);
    llayers = amxd_object_get_value(cstring_t, vlanterm_obj2, "LowerLayers", NULL);
    assert_string_equal(llayers, "");
    vlanterm_status = amxd_object_get_value(cstring_t, vlanterm_obj2, "Status", NULL);
    assert_string_equal(vlanterm_status, "Down");
    free(llayers);
    free(vlanterm_status);

    amxm_execute_function("mod-vlan-dummy", MOD_VLAN_CTRL, "get_last_destroy_arg", &data, &ret);
    const char* vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    assert_non_null(vlanport_name);
    assert_string_equal(vlanport_name, "vlan20");

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_wrong_reasons(UNUSED void** state) {
    assert_int_equal(_lastchange_on_read(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
    assert_int_equal(_ethernet_instance_removed(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
    assert_int_equal(_ethernet_vlanterm_removed(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
}

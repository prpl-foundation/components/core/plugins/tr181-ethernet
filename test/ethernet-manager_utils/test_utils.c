/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb_register.h>

#include <amxo/amxo.h>

#include <linux/sockios.h>

#include "../common/mock.h"
#include "../common/common_functions.h"

#include "ethernet.h"
#include "ethernet_utils.h"
#include "ethernet_mac.h"


#include "test_utils.h"


static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

static const char* odl_config = "../common/mock.odl";
static const char* odl_nm = "../common/mock_netmodel.odl";
static const char* odl_defs = "../../odl/ethernet-manager_definition.odl";
static const char* odl_mock_netdev = "mock_netdev_dm.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_netdev, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_nm, root_obj), 0);

    _ethernet_manager_main(AMXO_START, &dm, &parser);

    handle_events();

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    _ethernet_manager_main(AMXO_STOP, &dm, &parser);

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    handle_events();

    test_unregister_dummy_be();

    return 0;
}

void test_ethernet_info(UNUSED void** state) {
    int rv = -1;
    ethernet_info_t* ethernet = NULL;

    rv = ethernet_info_clean(&ethernet);
    assert_int_equal(rv, amxd_status_ok);

    rv = ethernet_info_new(NULL, NULL);
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    rv = ethernet_info_new(&ethernet, NULL);
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    rv = ethernet_info_new(&ethernet, "");
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    rv = ethernet_info_new(NULL, "eth0");
    assert_int_equal(rv, amxd_status_invalid_function_argument);

    rv = ethernet_info_new(&ethernet, "eth0");
    assert_int_equal(rv, amxd_status_ok);
    assert_non_null(ethernet);

    rv = ethernet_info_clean(NULL);
    assert_int_equal(rv, amxd_status_ok);

    rv = ethernet_info_clean(&ethernet);
    assert_int_equal(rv, amxd_status_ok);
    assert_null(ethernet);
}

void test_dm_ethernet_update_status(UNUSED void** state) {
    amxd_status_t rv = -1;
    ethernet_info_t* ethernet;
    char* status = NULL;
    char input_states[8][20] = {"notpresent", "down", "lowerlayerdown", "dormant", "up", "Error",
        "unknown", "invalid_state"};
    char output_states[8][20] = {"NotPresent", "Down", "LowerLayerDown", "Dormant", "Up", "Error",
        "Unknown", "Unknown"};

    rv = ethernet_info_new(&ethernet, "eth0" "test");
    assert_int_equal(rv, amxd_status_ok);
    assert_non_null(ethernet);
    ethernet->object = amxd_dm_findf(&dm, "Ethernet.Interface.ETH0.");
    assert_non_null(ethernet->object);
    for(int i = 0; i < 8; i++) {

        rv = dm_ethernet_update_status(ethernet, input_states[i]);
        assert_int_equal(rv, amxd_status_ok);
        handle_events();

        status = amxd_object_get_value(cstring_t, ethernet->object, "Status", NULL);
        assert_int_equal(strcmp(output_states[i], status), 0);
        free(status);
    }
    ethernet_info_clean(&ethernet);
    assert_int_equal(0, 0);
}

void test_netdev_state_subscription(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;

    obj = amxd_dm_findf(&dm, "Ethernet.VLANTermination.VLAN1.");
    char* status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Down");
    free(status);

    // Set netdev status to up, this will be the initial state
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.");
    amxd_trans_set_value(cstring_t, &trans, "State", "up");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Down");
    free(status);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.VLAN1.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    set_query_getResult_value("eth0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Up");
    free(status);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.");
    amxd_trans_set_value(cstring_t, &trans, "State", "dormant");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Dormant");
    free(status);
}

void test_netdev_link_added_subscription(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.VLANTermination.");
    amxd_trans_add_inst(&trans, 0, "VLAN10");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.1");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 10);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "VLAN20");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan20");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Link.1");
    amxd_trans_set_value(uint32_t, &trans, "VLANID", 20);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // If no interface exists in NetDev with a matching name the status will be Error
    obj = amxd_dm_findf(&dm, "Ethernet.VLANTermination.VLAN10.");
    char* status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Error");
    free(status);

    // Add the link to NetDev
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.");
    amxd_trans_add_inst(&trans, 0, "vlan10");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan10");
    amxd_trans_set_value(cstring_t, &trans, "State", "up");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // After adding the link, the Status should be updated
    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Up");
    free(status);

    obj = amxd_dm_findf(&dm, "Ethernet.VLANTermination.VLAN20.");
    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Error");
    free(status);

    // Add the second link to NetDev
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.");
    amxd_trans_add_inst(&trans, 0, "vlan20");
    amxd_trans_set_value(cstring_t, &trans, "Name", "vlan20");
    amxd_trans_set_value(cstring_t, &trans, "State", "up");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // After adding the second link, the Status should be updated
    status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(status, "Up");
    free(status);

}

void test_netdev_mac_subscription(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;

    // Adds a link interface so coverage for _netdev_new_link_added_cb
    // will be improved
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Link.");
    amxd_trans_add_inst(&trans, 0, "ETH0");
    amxd_trans_set_value(cstring_t, &trans, "Name", "eth0");
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Interfaces");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    obj = amxd_dm_findf(&dm, "Ethernet.Interface.ETH0.");
    assert_non_null(obj);
    char* mac = amxd_object_get_value(cstring_t, obj, "MACAddress", NULL);
    assert_non_null(mac);
    assert_string_equal(mac, "00:00:FA:CA:DE:00");
    free(mac);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.");
    amxd_trans_add_inst(&trans, 0, "eth0");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "Name", "eth0");
    amxd_trans_set_value(cstring_t, &trans, "LLAddress", "12:34:56:78:9A:BC");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    mac = amxd_object_get_value(cstring_t, obj, "MACAddress", NULL);
    assert_non_null(mac);
    assert_string_equal(mac, "12:34:56:78:9A:BC");
    free(mac);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.[Name == 'eth0']");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "LLAddress", "CB:A9:87:65:43:21");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    mac = amxd_object_get_value(cstring_t, obj, "MACAddress", NULL);
    assert_non_null(mac);
    assert_string_equal(mac, "CB:A9:87:65:43:21");
    free(mac);
}

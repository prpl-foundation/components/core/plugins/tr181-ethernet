/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "ethernet.h"
#include "ethernet_actions.h"
#include "ethernet_events.h"
#include "ethernet_rpc.h"

#include "../common/mock.h"
#include "../common/common_functions.h"

static const char* value = "";

amxd_status_t _dummy_dmstats_func(UNUSED amxd_object_t* object,
                                  UNUSED amxd_param_t* param,
                                  UNUSED amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  UNUSED amxc_var_t* const retval,
                                  UNUSED void* priv) {

    return amxd_status_ok;
}

static amxd_status_t _dummy_resolvePath(UNUSED amxd_object_t* object,
                                        UNUSED amxd_function_t* func,
                                        UNUSED amxc_var_t* args,
                                        amxc_var_t* ret) {
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, ret, "resolver");

    return amxd_status_ok;
}

static amxd_status_t _dummy_getResult(UNUSED amxd_object_t* object,
                                      UNUSED amxd_function_t* func,
                                      UNUSED amxc_var_t* args,
                                      amxc_var_t* ret) {
    amxc_var_set(cstring_t, ret, value);

    return amxd_status_ok;
}

static amxd_status_t _dummy_openQuery(UNUSED amxd_object_t* object,
                                      UNUSED amxd_function_t* func,
                                      UNUSED amxc_var_t* args,
                                      amxc_var_t* ret) {
    amxc_var_set(uint32_t, ret, 1);

    return amxd_status_ok;
}

static amxd_status_t _dummy_closeQuery(UNUSED amxd_object_t* object,
                                       UNUSED amxd_function_t* func,
                                       UNUSED amxc_var_t* args,
                                       UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

static void dummy_resolve_mod_dmstat_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_object_read",
                                            AMXO_FUNC(_dummy_dmstats_func)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_object_list",
                                            AMXO_FUNC(_dummy_dmstats_func)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_object_describe",
                                            AMXO_FUNC(_dummy_dmstats_func)), 0);
}

static void dummy_resolve_netmodel_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "resolvePath",
                                            AMXO_FUNC(_dummy_resolvePath)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "openQuery",
                                            AMXO_FUNC(_dummy_openQuery)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "closeQuery",
                                            AMXO_FUNC(_dummy_closeQuery)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "getResult",
                                            AMXO_FUNC(_dummy_getResult)), 0);
}

void resolver_add_all_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_is_empty_or_in",
                                            AMXO_FUNC(_check_is_empty_or_in)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "lastchange_on_read",
                                            AMXO_FUNC(_lastchange_on_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_instance_removed",
                                            AMXO_FUNC(_ethernet_instance_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_vlanterm_removed",
                                            AMXO_FUNC(_ethernet_vlanterm_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_interface_added",
                                            AMXO_FUNC(_ethernet_interface_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_link_added",
                                            AMXO_FUNC(_ethernet_link_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_enable_changed",
                                            AMXO_FUNC(_ethernet_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_vlanterm_added",
                                            AMXO_FUNC(_ethernet_vlanterm_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_vlanterm_enable_changed",
                                            AMXO_FUNC(_ethernet_vlanterm_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_llayer_changed",
                                            AMXO_FUNC(_ethernet_llayer_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_vlanid_changed",
                                            AMXO_FUNC(_ethernet_vlanid_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_vlanprio_changed",
                                            AMXO_FUNC(_ethernet_vlanprio_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_link_deleted",
                                            AMXO_FUNC(_ethernet_link_deleted)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_link_mac_changed",
                                            AMXO_FUNC(_ethernet_link_mac_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_check_interface_upstream",
                                            AMXO_FUNC(_ethernet_check_interface_upstream)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_set_interface_config",
                                            AMXO_FUNC(_ethernet_set_interface_config)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_set_mtu",
                                            AMXO_FUNC(_ethernet_set_mtu)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "EnableLink",
                                            AMXO_FUNC(_EnableLink)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_overlapping_vlantermination",
                                            AMXO_FUNC(_check_overlapping_vlantermination)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_set_interface_led",
                                            AMXO_FUNC(_ethernet_set_interface_led)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_rmonstats_added",
                                            AMXO_FUNC(_ethernet_rmonstats_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_rmonstats_intf_changed",
                                            AMXO_FUNC(_ethernet_rmonstats_intf_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ethernet_rmonstats_enable",
                                            AMXO_FUNC(_ethernet_rmonstats_enable)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "rmonstats_on_read",
                                            AMXO_FUNC(_rmonstats_on_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "rmonstats_instance_removed",
                                            AMXO_FUNC(_rmonstats_instance_removed)), 0);


    dummy_resolve_mod_dmstat_functions(parser);
    dummy_resolve_netmodel_functions(parser);
}

void set_query_getResult_value(const char* result) {
    value = result;
}

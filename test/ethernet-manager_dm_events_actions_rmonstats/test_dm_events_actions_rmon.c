/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>
#include <linux/sockios.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxb/amxb_register.h>

#include "../common/common_functions.h"

#include "ethernet.h"
#include "ethernet_events.h"
#include "ethernet_actions.h"

#include "../common/mock.h"
#include "test_dm_events_actions_rmon.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

static const char* odl_config = "../common/mock.odl";
static const char* odl_nm = "../common/mock_netmodel.odl";
static const char* odl_defs = "../../odl/ethernet-manager_definition.odl";
static const char* odl_mock_netdev = "mock_netdev_dm.odl";

static bool rmonstats_module_called = false;

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_netdev, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_nm, root_obj), 0);

    assert_int_equal(_ethernet_manager_main(AMXO_START, &dm, &parser), 0);

    handle_events();

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    _ethernet_manager_main(AMXO_STOP, &dm, &parser);

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    handle_events();

    test_unregister_dummy_be();

    return 0;
}

void test_rmonstats_create(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* rmonstats_obj = NULL;
    char* name = NULL;
    char* status = NULL;
    bool enable = false;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.Link.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_add_inst(&trans, 0, "ETH0");
    amxd_trans_set_value(cstring_t, &trans, "Name", "eth0");
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Ethernet.Interface.1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "LAN");
    amxd_trans_set_value(cstring_t, &trans, "Name", "br-lan");
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.Bridging.Bridge.1.Port.1");
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", "12:34:56:78:9A:BC");
    amxd_trans_set_value(bool, &trans, "Enable", true);

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.RMONStats.");
    amxd_trans_add_inst(&trans, 0, "test");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();

    rmonstats_obj = amxd_dm_findf(&dm, "Ethernet.RMONStats.test.");
    assert_non_null(rmonstats_obj);

    name = amxd_object_get_value(cstring_t, rmonstats_obj, "Name", NULL);
    assert_string_equal(name, "test");

    enable = amxd_object_get_value(bool, rmonstats_obj, "Enable", NULL);
    assert_false(enable);

    status = amxd_object_get_value(cstring_t, rmonstats_obj, "Status", NULL);
    assert_string_equal(status, RMONSTATS_STATUS_DISABLED);

    amxd_trans_clean(&trans);
    free(name);
    free(status);
}

void test_rmonstats_enable(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* rmonstats_obj = NULL;
    char* name = NULL;
    char* status = NULL;
    bool enable = false;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.RMONStats.test.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    rmonstats_obj = amxd_dm_findf(&dm, "Ethernet.RMONStats.test.");
    assert_non_null(rmonstats_obj);

    name = amxd_object_get_value(cstring_t, rmonstats_obj, "Name", NULL);
    assert_string_equal(name, "test");

    enable = amxd_object_get_value(bool, rmonstats_obj, "Enable", NULL);
    assert_true(enable);

    status = amxd_object_get_value(cstring_t, rmonstats_obj, "Status", NULL);
    assert_string_equal(status, RMONSTATS_STATUS_ERR_MISCONFIGURED);

    amxd_trans_clean(&trans);
    free(name);
    free(status);
}

void test_rmonstats_intf(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* rmonstats_obj = NULL;
    amxd_object_t* ethernet_object = NULL;
    char* name = NULL;
    char* status = NULL;
    bool enable = false;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.RMONStats.test.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Ethernet.Link.ETH0.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    rmonstats_obj = amxd_dm_findf(&dm, "Ethernet.RMONStats.test.");
    ethernet_object = amxd_dm_findf(&dm, "Ethernet.Link.ETH0.");
    assert_non_null(rmonstats_obj);
    assert_non_null(ethernet_object);

    name = amxd_object_get_value(cstring_t, rmonstats_obj, "Name", NULL);
    assert_string_equal(name, "test");

    enable = amxd_object_get_value(bool, rmonstats_obj, "Enable", NULL);
    assert_true(enable);

    status = amxd_object_get_value(cstring_t, rmonstats_obj, "Status", NULL);
    assert_string_equal(status, RMONSTATS_STATUS_ENABLED);

    amxd_trans_clean(&trans);
    free(name);
    free(status);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Ethernet.RMONStats.test.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Ethernet.Link.wrong.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    rmonstats_obj = amxd_dm_findf(&dm, "Ethernet.RMONStats.test.");
    assert_non_null(rmonstats_obj);

    name = amxd_object_get_value(cstring_t, rmonstats_obj, "Name", NULL);
    assert_string_equal(name, "test");

    enable = amxd_object_get_value(bool, rmonstats_obj, "Enable", NULL);
    assert_true(enable);

    status = amxd_object_get_value(cstring_t, rmonstats_obj, "Status", NULL);
    assert_string_equal(status, RMONSTATS_STATUS_ERR_MISCONFIGURED);

    amxd_trans_clean(&trans);
    free(name);
    free(status);
}

void test_rmonstats_execute_module(UNUSED void** state) {
    amxd_object_t* rmonstats_obj = NULL;
    char* name = NULL;

    rmonstats_obj = amxd_dm_findf(&dm, "Ethernet.RMONStats.test.");
    assert_non_null(rmonstats_obj);
    handle_events();

    name = amxd_object_get_value(cstring_t, rmonstats_obj, "Name", NULL);
    assert_string_equal(name, "test");

    handle_events();

    assert_true(rmonstats_module_called);

    free(name);
}

const char* __wrap_mod_ctrl_get(UNUSED amxd_object_t* obj, UNUSED const char* ctrl_param) {
    return "rmonstats-test-ctrl";
}

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 UNUSED const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {

    if(strcmp(shared_object_name, "rmonstats-test-ctrl") == 0) {
        rmonstats_module_called = true;
    }
    return 0;
}
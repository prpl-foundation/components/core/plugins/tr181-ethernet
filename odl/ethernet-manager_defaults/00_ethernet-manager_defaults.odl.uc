%populate {
    object Ethernet.Link {
        instance add (Alias = "link_lo", Name = "lo") {
            parameter Enable = "true";
        }
{% if (BDfn.hasAnyUpstream()) : %}
{% let moca_count=0; let xpon_count=0; let ethernet_count=0; let cellular_count=0 %}
{# The default wan interface needs to be set first (ip manager refers to Ethernet.Link.2.)#}
{% let DefaultItf = BD.Interfaces[BDfn.getUpstreamInterfaceIndex()] %}
{% IntfIndex = BDfn.getInterfaceIndex(DefaultItf.Name, DefaultItf.Type); %}
        instance add ("{{DefaultItf.Type}}_wan", Name = "{{DefaultItf.Name}}") {
{% if (DefaultItf.Type == "moca") : %}{% moca_count++ %}
            parameter LowerLayers = "Device.MoCA.Interface.{{IntfIndex + 1}}";
{% elif (DefaultItf.Type == "xpon") : %}{% xpon_count++ %}
            parameter LowerLayers = "Device.XPON.ONU.1.EthernetUNI.1";
{% elif (DefaultItf.Type == "cellular") : %}{% cellular_count++ %}
            parameter LowerLayers = "Device.Cellular.Interface.{{IntfIndex + 1}}";
{% else %}{% ethernet_count++ %}
            parameter LowerLayers = "Device.Ethernet.Interface.{{IntfIndex + 1}}";
{% endif; %}
            parameter Enable = "true";
        }
{# loop over all other interfaces (except the default wan interface) #}
{% for (let Itf in BD.Interfaces) : if (Itf.Upstream == "true" && Itf.Name != DefaultItf.Name) : %}
{% IntfIndex = BDfn.getInterfaceIndex(Itf.Name, Itf.Type); %}
{% if (Itf.Type == "moca") : %}{% moca_count++ %}
        instance add ("{{Itf.Type}}_wan{% if (moca_count > 1) : %}_{{ moca_count }}{% endif; %}", Name = "{{Itf.Name}}") {
            parameter LowerLayers = "Device.MoCA.Interface.{{IntfIndex + 1}}";
{% elif (Itf.Type == "xpon") : %}{% xpon_count++ %}
        instance add ("{{Itf.Type}}_wan{% if (xpon_count > 1) : %}_{{ xpon_count }}{% endif; %}", Name = "{{Itf.Name}}") {
            parameter LowerLayers = "Device.XPON.ONU.1.EthernetUNI.1";
{% elif (Itf.Type == "cellular") : %}{% cellular_count++ %}
        instance add ("{{Itf.Type}}_wan{% if (cellular_count > 1) : %}_{{ cellular_count }}{% endif; %}", Name = "{{Itf.Name}}") {
            parameter LowerLayers = "Device.Cellular.Interface.{{IntfIndex + 1}}";
{% else %}{% ethernet_count++ %}
        instance add ("{{Itf.Type}}_wan{% if (ethernet_count > 1) : %}_{{ ethernet_count }}{% endif; %}", Name = "{{Itf.Name}}") {
            parameter LowerLayers = "Device.Ethernet.Interface.{{IntfIndex + 1}}";
{% endif; %}
        }
{% endif; endfor; %}
{% endif; %}
{% let i = 0 %}
{% for (let Bridge in BD.Bridges) : %}
{% i++ %}
        instance add ("bridge_{{lc(Bridge)}}", Name = "{{BD.Bridges[Bridge].Name}}") {
            parameter Enable = "true";
            parameter LowerLayers = "Device.Bridging.Bridge.{{i}}.Port.1";
        }
{% endfor; %}
    }
}


{% let SwitchPortIndex = 1 %}
%populate {
    object Ethernet.Interface {
{%	for ( let Itf in BD.Interfaces ) : if ( Itf.Type == "ethernet" ) : %}
        instance add(Alias = "{{Itf.Alias}}", Name = "{{Itf.Name}}") {
            parameter Enable = "true";
            {% if (Itf.Upstream) : %}
            parameter Upstream = "true";
            {% endif %}
            {% if (Itf.Description) : %}
            parameter "${prefix_}Description" = "{{ Itf.Description }}";
            {% endif %}
            {% if (Itf.SwitchPort) : %}
            parameter SwitchPort = "Switch.Port.{{SwitchPortIndex}}";
            parameter DefaultController = "mod-eth-switch";
            parameter RMONStatsController = "mod-switch-stats";
            parameter LEDController = "mod-switch-led";
            {% SwitchPortIndex++ %}
            {% endif %}
            {% if (Itf.CPUPort) : %}
            parameter CPUPort = "true";
            {% endif %}
        }
{% endif; endfor; %}
    }
}

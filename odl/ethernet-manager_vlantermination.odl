%define {
    select Ethernet {
        /**
        * VLAN Termination table (a stackable interface object as described in [Section 4.2/TR-181i2]).
        * A VLAN Termination entry is typically stacked on top of a Link object to receive and send
        * frames with the configured VLANID.
        *
        * At most one entry in this table can exist with a given value for Alias, or with a given value
        * for Name. On creation of a new table entry, the Agent MUST choose initial values for Alias
        * and Name such that the new entry does not conflict with any existing entries.
        * The non-functional key parameters Alias and Name are immutable and therefore MUST NOT
        * change once they've been assigned.
        *
        * @version 1.0
        */
        %persistent object VLANTermination[] {
            on action destroy call ethernet_vlanterm_removed;
            on action add-inst call assign_default_string "Name";

            /**
            * The number of entries in the VLANTermination table.
            *
            * @version 1.0
            */
            counted with VLANTerminationNumberOfEntries;

            /**
            * Enables or disables the VLANTermination entry.
            *
            * This parameter is based on ifAdminStatus from [RFC2863].
            *
            * @version 1.0
            */
            %persistent bool Enable {
                on action validate call check_overlapping_vlantermination;
                userflags %upc;
            }

            /**
            * The current operational state of the VLANTermination entry (see [Section 4.2.2/TR-181i2]).
            * Enumeration of:
            *   Up
            *   Down
            *   Unknown
            *   Dormant
            *   NotPresent
            *   LowerLayerDown
            *   Error
            *
            * When Enable is false then this parameter SHOULD normally be Down
            * (or NotPresent or Error if there is a fault condition on the interface).
            * When Enable is changed to true then this parameter SHOULD change to Up if and
            * only if the interface is able to transmit and receive network traffic;
            * it SHOULD change to Dormant if and only if the interface is
            * operable but is waiting for external actions before it can transmit and receive
            * network traffic (and subsequently change to Up if still operable when the
            * expected actions have completed);
            * it SHOULD change to LowerLayerDown if and only if the interface
            * is prevented from entering the Up state because one or more of the interfaces
            * beneath it is down;
            * it SHOULD remain in the Error state if there is an error or other fault
            * condition detected on the interface;
            * it SHOULD remain in the NotPresent state if the interface has
            * missing (typically hardware) components;
            * it SHOULD change to Unknown if the state of the interface can not be determined
            * for some reason.
            *
            * This parameter is based on ifOperStatus from [RFC2863].
            *
            * @version 1.0
            */
            %read-only string Status = "Down" {
                on action validate call check_enum
                    ["NotPresent", "Down", "LowerLayerDown", "Dormant", "Up", "Error", "Unknown"];
            }

            /**
            * A non-volatile unique key used to reference this instance. Alias provides a
            * mechanism for a Controller to label this instance for future reference.
            *
            * The following mandatory constraints MUST be enforced:
            *   The value MUST NOT be empty.
            *   The value MUST start with a letter.
            *   If the value is not assigned by the Controller at creation time,
            *   the Agent MUST assign a value with an "cpe-" prefix.
            *
            * The value MUST NOT change once it's been assigned.
            *
            * @version 1.0
            */
            %persistent %unique %key string Alias {
                on action validate call check_maximum_length 64;
                userflags %upc;
            }

            /**
            * The textual name of the VLANTermination entry as assigned by the CPE.
            *
            * @version 1.0
            */
            %persistent %read-only string Name {
                on action validate call check_maximum_length 64;
                userflags %upc;
            }

            /**
            * The accumulated time in seconds since the VLANTermination entry entered its current
            * operational state
            *
            * @version 1.0
            */
            %read-only uint32 LastChange {
                on action read call lastchange_on_read;
            }

            /**
            * Normally this is a Comma-separated list (maximum list length 1024) of strings.
            * We only support a single path to a Ethernet Link instance
            * Each list item MUST be the Path Name of an interface object that is stacked
            * immediately below this interface object. If the referenced object is deleted,
            * the corresponding item MUST be removed from the list.
            * See [Section 4.2.1/TR-181i2].
            *
            * When ManagementPort is set to true the CPE MUST set LowerLayers to reference
            * all non-management VLANTermination entrys that are within the same Bridge instance
            * (and update LowerLayers when subsequent non-management VLANTermination entrys are added
            * or deleted on that Bridge).
            * The Controller SHOULD NOT set LowerLayers in this case.
            *
            * @version 1.0
            */
            %persistent string LowerLayers {
                on action write call set_object_ref_simple;
                on action validate call check_maximum_length 1024;
                on action validate call check_overlapping_vlantermination;
                userflags %upc;
            }

            /**
            * The VLAN ID for this VLANTermination entry (as defined in [802.1Q-2011]).
            * Only ingress frames with this VLAN ID will be passed to higher protocol layers;
            * frames sent from higher protocol layers will be tagged with this VLAN ID.
            *
            * @version 1.0
            */
            %persistent uint32 VLANID = 1 {
                on action validate call check_range [1, 4094];
                on action validate call check_overlapping_vlantermination;
                userflags %upc;
            }

            /**
            * The Tag Protocol Identifier (TPID) assigned to this VLANTermination.
            * The TPID is an EtherType value used to identify the frame as a tagged frame.
            *
            * Standard [Table 9.1/802.1Q-2011] TPID values are:
            *   S-TAG 0x88A8 = 34984
            *   C-TAG 0x8100 = 33024
            *
            * Non-Standard TPID values are:
            *   S-TAG 0x9100 = 37120
            *
            * @version 1.0
            */
            %persistent uint32 TPID = 33024 {
                userflags %upc;
            }

            /**
            * Vlan egress priority, number between 0 and 7
            *
            * @version 1.0
            */
            %persistent uint32 VLANPriority {
                on action validate call check_range [0, 7];
                userflags %upc;
            }

            /**
            * Throughput statistics for this interface.
            *
            * The CPE MUST reset the interface's Stats parameters
            * (unless otherwise stated in individual object or parameter descriptions) either
            * when the interface becomes operationally down due to a previous administrative
            * down (i.e. the interface's Status parameter transitions to a down state after
            * the interface is disabled) or when the interface becomes administratively up
            * (i.e. the interface's Enable parameter transitions from false to true).
            * Administrative and operational interface status is discussed in [Section 4.2.2/TR-181i2].
            *
            * @version 1.0
            */
            object Stats {
                on action read call stats_object_read;
                on action list call stats_object_list;
                on action describe call stats_object_describe;
            }
        }
    }
}

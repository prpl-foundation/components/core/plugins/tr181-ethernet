%define {
    select Ethernet {
        /**
         * Ethernet statistics based on the [RFC2819] RMON-MIB etherStatsTable, with some extensions inspired by [Section 9.3.32/G.988].
         * Each instance is associated with an interface capable of transporting Ethernet-encapsulated packets, and contains a set of unidirectional Ethernet statistics.
         * The statistics are sampled either on ingress or on egress. This is determined as follows:
         * If the instance is associated with an egress queue (or queues) via the Queue parameter or by setting AllQueues to true then data is sampled on egress. In this case Bytes etc measure the data that has been sent on the interface, possibly filtered by Queue or VLANID.
         * Otherwise data is sampled on ingress. In this case Bytes etc measure the data that has been received on the interface, possibly filtered by VLANID.
         * When sampling on egress, the term received means received by the queuing sub-system.
         * Multiple instances can be associated with a single interface: individual instances can be configured to collect data associated with the entire interface, or with a particular VLAN and/or queue.
         * The CPE MUST reset each instances’s Stats parameters whenever the instance is disabled and re-enabled. Whether this reset occurs when the instance becomes operationally disabled (Status = Disabled) or administratively enabled (Enable = true) is a local matter to the CPE. This is similar to the behavior of interface statistics, e.g. as specified for Interface.{i}.Stats. Furthermore, this instance’s Stats parameters MUST be reset whenever the referenced interface’s Stats parameters are reset, or when the referenced queue or VLAN is disabled and re-enabled.
         * For enabled table entries, if Interface references an interface that is not capable of transporting Ethernet-encapsulated packets, or if Queue references a queue that is not instantiated on Interface, or if Queue is not a valid reference and AllQueues is false, the table entry is inoperable and the CPE MUST set Status to Error_Misconfigured.
         * Note: The RMONStats table includes unique key parameters that are strong references. If a strongly referenced object is deleted, the CPE will set the referencing parameter to an empty string. However, doing so under these circumstances might cause the updated RMONStats row to then violate the table’s unique key constraint; if this occurs, the CPE MUST set Status to Error_Misconfigured and disable the offending RMONStats row.
         * At most one entry in this table (regardless of whether or not it is enabled) can exist with a given value for Alias. On creation of a new table entry, the Agent MUST choose an initial value for Alias such that the new entry does not conflict with any existing entries. At most one enabled entry in this table can exist with the same values for all of Interface, VLANID and Queue.
         * @version 2.4
         */
        object RMONStats[] {
            on action read call rmonstats_on_read;
            on action destroy call rmonstats_instance_removed;

            /**
             * The number of entries in the RMONStats table.
             * @version 2.4
             */
            counted with RMONStatsNumberOfEntries;

            /**
             * Enables or disables this instance.
             * @version 2.4
             */
            bool Enable = false;

            /**
             * The status of this instance. Enumeration of:
             * - Disabled
             * - Enabled
             * - Error_Misconfigured
             * - Error (OPTIONAL)
             * The Error_Misconfigured value indicates that a necessary configuration value is undefined or invalid.
             * The Error value MAY be used by the CPE to indicate a locally defined error condition.
             * @version 2.4
             */
            %read-only string Status {
                default "Disabled";
                on action validate call check_enum ["Disabled", "Enabled", "Error_Misconfigured", "Error"];
            }

            /**
             * [Alias] A non-volatile handle used to reference this instance. Alias provides a mechanism for an ACS to label this instance for future reference.
             * If the CPE supports the Alias-based Addressing feature as defined in [Section 3.6.1/TR-069] and described in [Appendix II/TR-069], the following mandatory constraints MUST be enforced:
             * Its value MUST NOT be empty.
             * Its value MUST start with a letter.
             * If its value is not assigned by the ACS, it MUST start with a “cpe-” prefix.
             * The CPE MUST NOT change the parameter value.
             * At most one entry in this table (regardless of whether or not it is enabled) can exist with a given value for Alias. On creation of a new table entry, the Agent MUST choose an initial value for Alias such that the new entry does not conflict with any existing entries. At most one enabled entry in this table can exist with the same values for all of Interface, VLANID and Queue.
             * The Agent MUST choose an initial value that doesn’t conflict with any existing entries.
             * version 2.4
             */
            %unique %key string Alias {
                on action validate call check_maximum_length 64;
            }

            /**
             * The textual name of the RMONStats entry as assigned by the CPE.
             * @version 2.4
             */
            %read-only string Name {
                on action validate call check_maximum_length 64;
            }

            /**
             * The interface associated with this instance.
             * The value MUST be the Path Name of an interface that is capable of transporting Ethernet-encapsulated packets.
             * The term “capable of transporting Ethernet-encapsulated packets” means “has an Ethernet header” and therefore refers to any interface that is at or below an Ethernet.Link instance in the interface stack.
             * @version 2.4
             */
            string Interface {
                on action write call set_object_ref_simple;
            }

            /**
             * Filter criterion.
             * The VLAN ID for which statistics are to be collected.
             * A zero value indicates that all packets, whether or not they have a VLAN header, will be considered.
             * A non-zero value indicates that only packets that have the the specified VLAN ID will be considered.
             * @version 2.4
             */
            uint16 VLANID = 0 {
                on action validate call check_maximum 4094;
            }

            /**
             * The value MUST be the Path Name of a row in the QoS.Queue. table. If the referenced object is deleted, the parameter value MUST be set to an empty string. Filter criterion.
             * The egress queue with which this instance is associated.
             * Only packets that are sent to the referenced queue will be considered.
             * @version 2.4
             */
            string Queue;

            /**
             * Indicates whether this instance applies to all queues. If true, the value of Queue is ignored since all egress queues are indicated.
             * @version 2.4
             */
            bool AllQueues = false;

            /**
             * The total number of events in which packets were dropped due to lack of resources. Note that this number is not necessarily the number of packets dropped; it is just the number of times this condition has been detected.
             * This parameter is based on etherStatsDropEvents from [RFC2819].
             * @version 2.4
             */
            %read-only uint32 DropEvents = 0;

            /**
             * The total number of bytes (including those in bad packets) received (excluding framing bits but including FCS bytes).
             * This parameter is based on etherStatsOctets from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 Bytes = 0;

            /**
             * The total number of packets (including bad packets, broadcast packets, and multicast packets) received.
             * This parameter is based on etherStatsPkts from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 Packets = 0;

            /**
             * The total number of good packets received that were directed to the broadcast address. Note that this does not include multicast packets.
             * This parameter is based on etherStatsBroadcastPkts from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 BroadcastPackets = 0;

            /**
             * The total number of good packets received that were directed to a multicast address. Note that this number does not include packets directed to the broadcast address.
             * This parameter is based on etherStatsMulticastPkts from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 MulticastPackets = 0;

            /**
             * The total number of packets received that had a length (excluding framing bits, but including FCS bytes) of between 64 and 1518 bytes, inclusive, but had either a bad Frame Check Sequence (FCS) with an integral number of bytes (FCS Error) or a bad FCS with a non-integral number of bytes (Alignment Error).
             * This parameter is based on etherStatsCRCAlignErrors from [RFC2819].
             * @version 2.4
             */
            %read-only uint32 CRCErroredPackets = 0;

            /**
             * The total number of packets received that were less than 64 bytes long (excluding framing bits, but including FCS bytes) and were otherwise well formed.
             * This parameter is based on etherStatsUndersizePkts from [RFC2819].
             * @version 2.4
             */
            %read-only uint32 UndersizePackets = 0;

            /**
             * The total number of packets received that were longer than 1518 bytes (excluding framing bits, but including FCS bytes) and were otherwise well formed.
             * This parameter is based on etherStatsOversizePkts from [RFC2819].
             * @version 2.4
             */
            %read-only uint32 OversizePackets = 0;

            /**
             * The total number of packets (including bad packets) received that were 64 bytes in length (excluding framing bits but including FCS bytes).
             * This parameter is based on etherStatsPkts64Octets from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 Packets64Bytes = 0;

            /**
             * The total number of packets (including bad packets) received that were between 65 and 127 bytes in length inclusive (excluding framing bits but including FCS bytes).
             * This parameter is based on etherStatsPkts65to127Octets from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 Packets65to127Bytes = 0;

            /**
             * The total number of packets (including bad packets) received that were between 128 and 255 bytes in length inclusive (excluding framing bits but including FCS bytes).
             * This parameter is based on etherStatsPkts6128to255Octets from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 Packets128to255Bytes = 0;

            /**
             * The total number of packets (including bad packets) received that were between 256 and 511 bytes in length inclusive (excluding framing bits but including FCS bytes).
             * This parameter is based on etherStatsPkts256to511Octets from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 Packets256to511Bytes = 0;

            /**
             * The total number of packets (including bad packets) received that were between 512 and 1023 bytes in length inclusive (excluding framing bits but including FCS bytes).
             * This parameter is based on etherStatsPkts512to1023Octets from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 Packets512to1023Bytes = 0;

            /**
             * The total number of packets (including bad packets) received that were between 1024 and 1518 bytes in length inclusive (excluding framing bits but including FCS bytes).
             * This parameter is based on etherStatsPkts1024to1518Octets from [RFC2819].
             * @version 2.4
             */
            %read-only uint64 Packets1024to1518Bytes = 0;
        }
    }
}

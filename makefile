include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C mod-vlan-dummy/src all
	$(MAKE) -C mod-eth-lla/src all
	$(MAKE) -C mod-led-dummy/src all
	$(MAKE) -C mod-netdev-stats/src all
	$(MAKE) -C mod-eth-switch/src all
	$(MAKE) -C mod-switch-stats/src all
	$(MAKE) -C mod-switch-led/src all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C mod-vlan-dummy/src clean
	$(MAKE) -C mod-eth-lla/src clean
	$(MAKE) -C mod-led-dummy/src clean
	$(MAKE) -C mod-netdev-stats/src clean
	$(MAKE) -C mod-eth-switch/src clean
	$(MAKE) -C mod-switch-stats/src clean
	$(MAKE) -C mod-switch-led/src clean
	$(MAKE) -C odl clean
	$(MAKE) -C test clean
	$(MAKE) -C doc clean

install: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-vlan-dummy.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-vlan-dummy.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-netdev-stats.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-netdev-stats.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-eth-lla.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-eth-lla.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-led-dummy.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-led-dummy.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-eth-switch.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-eth-switch.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-switch-stats.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-switch-stats.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-switch-led.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-switch-led.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-ethernet_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_interface.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_interface.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_link.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_link.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vlantermination.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_vlantermination.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_rmonstats.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_rmonstats.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_config.odl.uc $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_config.odl.uc
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_default_mac_addresses.odl.uc $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_default_mac_addresses.odl.uc
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-vlan-dummy.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-vlan-dummy.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-netdev-stats.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-netdev-stats.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-eth-lla.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-eth-lla.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-led-dummy.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-led-dummy.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-eth-switch.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-eth-switch.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-switch-stats.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-switch-stats.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-switch-led.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-switch-led.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-ethernet_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_interface.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_interface.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_link.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_link.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vlantermination.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_vlantermination.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_rmonstats.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_rmonstats.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_config.odl.uc $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_config.odl.uc
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_default_mac_addresses.odl.uc $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_default_mac_addresses.odl.uc
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc

	$(eval ODLFILES += odl/$(COMPONENT).odl)
	$(eval ODLFILES += odl/$(COMPONENT)_definition.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C mod-eth-lla/test run
	$(MAKE) -C mod-eth-lla/test coverage
	$(MAKE) -C mod-netdev-stats/test run
	$(MAKE) -C mod-netdev-stats/test coverage
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test
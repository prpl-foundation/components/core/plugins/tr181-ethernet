/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ethernet.h"
#include "ethernet_utils.h"
#include "ethernet_ethtool_utils.h"
#include "ethernet_soc_utils.h"
#include "ethernet_events.h"
#include "ethernet_mac.h"
#include "ethernet_switch.h"

#define ME "events"

static rmonstate_states_conv_t rmonstats_states_table[] = {
    {RMONSTATS_STATUS_DISABLED, RMONSTATS_DISABLED},
    {RMONSTATS_STATUS_ENABLED, RMONSTATS_ENABLED},
    {RMONSTATS_STATUS_ERR_MISCONFIGURED, RMONSTATS_ERR_MISCONFIGURED},
    {RMONSTATS_STATUS_ERR, RMONSTATS_ERR},
    {NULL, RMONSTATS_NUM_STATUS}
};

static int _ethernet_vlanterm_disable_cb(UNUSED amxd_object_t* templ,
                                         amxd_object_t* instance,
                                         UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    when_null(instance, exit);

    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "");
    rv = amxd_trans_apply(&trans, ethernet_get_dm());

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void ethernet_link_netdevname_updated(const char* sig,
                                             const amxc_var_t* data,
                                             void* priv) {
    SAH_TRACEZ_IN(ME);
    ethernet_info_t* ethernet = (ethernet_info_t*) priv;
    const char* new_name = NULL;
    when_null_trace(ethernet, exit, ERROR, "Private data is NULL");

    // Check if query is opened correctly
    if(amxc_var_is_null(data)) {
        if(strcmp(sig, "Resolver") == 0) {
            SAH_TRACEZ_INFO(ME, "Query interface path is still being resolved");
        } else {
            SAH_TRACEZ_ERROR(ME, "Query is closed in NetModel, closing local query");
            netmodel_closeQuery(ethernet->query);
            ethernet->query = NULL;
        }
        goto exit;
    }

    new_name = amxc_var_constcast(cstring_t, data);
    when_null_trace(new_name, exit, ERROR, "Received NULL instead of a string");
    SAH_TRACEZ_INFO(ME, "Ethernet Link name changing from %s to %s",
                    ethernet->link_name != NULL ? ethernet->link_name : "", new_name);

    if(!str_empty(ethernet->link_name)) {
        // We have a link name
        if(str_empty(new_name) || (strcmp(ethernet->link_name, new_name) != 0)) {
            // The new name is empty or different than the current one -> remove current vlan
            mod_vlan_execute_function("destroy-vlan", ethernet);
            amxb_subscription_delete(&(ethernet->netdev_status_subscription));
            free(ethernet->link_name);
            ethernet->link_name = NULL;
        } else {
            // The new name is identical to the current one -> do nothing
            goto exit;
        }
    }

    // The current name will always be empty at this point (was empty or we just cleared it)
    // If the new name is not empty -> set it and create the vlan
    if(!str_empty(new_name)) {
        ethernet->link_name = strdup(new_name);
        mod_vlan_execute_function("create-vlan", ethernet);
        netdev_link_state_subscription_new(ethernet);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static int ethernet_create_nm_query(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* llayers = amxd_object_get_value(cstring_t, ethernet->object, "LowerLayers", NULL);
    when_null_trace(llayers, exit, ERROR, "Failed to get LowerLayers parameter for %s", ethernet->netdev_intf);

    if(ethernet->query != NULL) {
        netmodel_closeQuery(ethernet->query);
        ethernet->query = NULL;
    }

    ethernet->query = netmodel_openQuery_getFirstParameter(llayers,
                                                           "ethernet-manager",
                                                           "NetDevName",
                                                           "netdev-bound",
                                                           "down",
                                                           ethernet_link_netdevname_updated,
                                                           ethernet);

    when_null_trace(ethernet->query, exit, ERROR,
                    "Failed to create netmodel query for NetDevName on intf %s", llayers);
    rv = 0;

exit:
    free(llayers);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void ethernet_close_nm_query(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    netmodel_closeQuery(ethernet->query);
    ethernet->query = NULL;
    free(ethernet->link_name);
    ethernet->link_name = NULL;

    SAH_TRACEZ_OUT(ME);
}

static void ethernet_netdev_init(ethernet_info_t* ethernet) {
    netdev_link_added_subscription_new(ethernet);
    netdev_link_state_subscription_new(ethernet);
    netdev_link_mac_address_subscription_new(ethernet);
}

void _ethernet_interface_added(UNUSED const char* const sig_name,
                               const amxc_var_t* const data,
                               UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_dm_t* dm = ethernet_get_dm();
    ethernet_info_t* ethernet = NULL;
    amxd_object_t* interface_templ_obj = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* interface_obj = NULL;
    const char* intf_name = GETP_CHAR(data, "parameters.Name");
    bool enable = GETP_BOOL(data, "parameters.Enable");
    amxo_parser_t* parser = ethernet_get_parser();
    const char* prefix = GETP_CHAR(&parser->config, "prefix_");
    const char* switch_port = NULL;
    amxc_string_t led;

    amxc_string_init(&led, 0);

    when_null_trace(interface_templ_obj, exit, ERROR, "Could not get template object");
    interface_obj = amxd_object_get_instance(interface_templ_obj, NULL, GET_UINT32(data, "index"));
    when_null_trace(interface_obj, exit, ERROR, "Could not get the added instance");

    when_not_null(interface_obj->priv, exit);

    rv = ethernet_info_new(&ethernet, intf_name);
    when_failed(rv, exit);

    interface_obj->priv = ethernet;
    ethernet->object = interface_obj;
    ethernet->type = ETHERNET_INTERFACE;

    // Query NetDev or Switch
    switch_port = GET_CHAR(amxd_object_get_param_value(interface_obj, "SwitchPort"), NULL);
    if((switch_port == NULL) || (*switch_port == 0)) {
        ethernet_netdev_init(ethernet);
    } else {
        ethernet_switch_init(interface_obj, ethernet, switch_port);
    }

    amxc_string_setf(&led, "%sLED", prefix);
    ethernet->led_object = amxd_object_get_child(interface_obj, amxc_string_get(&led, 0));
    when_null_trace(ethernet->led_object, exit, ERROR, "Could not get the LED object instance");
    ethernet->led_object->priv = ethernet;

    rv = ethernet_info_set_enabled(ethernet, enable);
    when_failed_trace(rv, exit, ERROR, "Failed to %s interface '%s': %d",
                      enable ? "enable" : "disable", ethernet->netdev_intf, rv);

    rv = dm_ethernet_ethtool_set(ethernet);
    when_failed_trace(rv, exit, ERROR, "Failed to write bitrate and duplex mode for '%s'", ethernet->netdev_intf);

    rv = mod_ethernet_set_mtu(ethernet);
    when_failed_trace(rv, exit, ERROR, "Failed to set MTU for '%s", ethernet->netdev_intf);

    rv = mod_ethernet_set_led(ethernet);
    when_failed_trace(rv, exit, ERROR, "Failed to set LED for '%s", ethernet->netdev_intf);

exit:
    amxc_string_clean(&led);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_link_added(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = ethernet_get_dm();
    ethernet_info_t* ethernet = NULL;
    amxd_object_t* link_templ_obj = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* link_obj = NULL;
    const char* intf_name = GETP_CHAR(data, "parameters.Name");
    bool enable = GETP_BOOL(data, "parameters.Enable");
    bool noarp = GETP_BOOL(data, "parameters.NoARP");
    int ret = -1;

    when_str_empty_trace(intf_name, exit, ERROR, "Link Name not set");
    when_null_trace(link_templ_obj, exit, ERROR, "Could not get template object");
    link_obj = amxd_object_get_instance(link_templ_obj, NULL, GET_UINT32(data, "index"));
    when_null_trace(link_obj, exit, ERROR, "Could not get the added instance");

    when_not_null(link_obj->priv, exit);

    rv = ethernet_info_new(&ethernet, intf_name);
    when_failed(rv, exit);

    link_obj->priv = ethernet;
    ethernet->object = link_obj;
    ethernet->type = ETHERNET_LINK;

    handle_mac_address(ethernet);

    netdev_link_added_subscription_new(ethernet);
    netdev_link_state_subscription_new(ethernet);

    link_disable_arp(ethernet, noarp);
    ret = ethernet_info_set_enabled(ethernet, enable);
    when_failed_trace(ret, exit, ERROR, "Failed to %s interface '%s': %d",
                      enable ? "enable" : "disable", ethernet->netdev_intf, ret);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_enable_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;
    amxd_object_t* obj = amxd_dm_signal_get_object(ethernet_get_dm(), data);
    ethernet_info_t* ethernet = NULL;
    bool enable = GETP_BOOL(data, "parameters.Enable.to");

    when_null_trace(obj, exit, ERROR, "Could not get object");
    ethernet = (ethernet_info_t*) obj->priv;
    when_null_trace(ethernet, exit, ERROR, "Object private data is null");

    ret = ethernet_info_set_enabled(ethernet, enable);
    when_failed_trace(ret, exit, ERROR, "Failed to %s interface '%s': %d",
                      enable ? "enable" : "disable", ethernet->netdev_intf, ret);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_link_noarp_changed(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;
    amxd_object_t* obj = amxd_dm_signal_get_object(ethernet_get_dm(), data);
    ethernet_info_t* ethernet = NULL;
    bool noarp = GETP_BOOL(data, "parameters.NoARP.to");

    when_null_trace(obj, exit, ERROR, "Could not get object");
    ethernet = (ethernet_info_t*) obj->priv;
    when_null_trace(ethernet, exit, ERROR, "Object private data is null");

    ret = link_disable_arp(ethernet, noarp);
    when_failed_trace(ret, exit, ERROR, "Failed to %s interface '%s': %d",
                      noarp ? "NoARP" : "ARP", ethernet->netdev_intf, ret);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_vlanterm_added(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = ethernet_get_dm();
    ethernet_info_t* ethernet = NULL;
    amxd_object_t* vlanterm_templ_obj = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* vlanterm_obj = NULL;

    when_null_trace(vlanterm_templ_obj, exit, ERROR, "Could not get template object");
    vlanterm_obj = amxd_object_get_instance(vlanterm_templ_obj, NULL, GET_UINT32(data, "index"));
    when_null_trace(vlanterm_obj, exit, ERROR, "Could not get the added instance");
    when_not_null(vlanterm_obj->priv, exit);

    rv = ethernet_info_new(&ethernet, GETP_CHAR(data, "parameters.Name"));
    when_failed_trace(rv, exit, ERROR, "failed to create info structure");

    vlanterm_obj->priv = ethernet;
    ethernet->object = vlanterm_obj;
    ethernet->type = ETHERNET_VLANTERMINATION;

    netdev_link_added_subscription_new(ethernet);

    if(GETP_BOOL(data, "parameters.Enable")) {
        ethernet_create_nm_query(ethernet);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_vlanterm_enable_changed(UNUSED const char* const sig_name,
                                       const amxc_var_t* const data,
                                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = ethernet_get_dm();
    ethernet_info_t* ethernet = NULL;
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);

    SAH_TRACEZ_INFO(ME, "Enable changed from \"%d\" to \"%d\"",
                    GETP_BOOL(data, "parameters.Enable.from"),
                    GETP_BOOL(data, "parameters.Enable.to"));

    when_null_trace(object, exit, ERROR, "could not get object");
    when_null_trace(object->priv, exit, ERROR, "object has no private data");
    ethernet = (ethernet_info_t*) object->priv;

    if(GETP_BOOL(data, "parameters.Enable.to")) {
        ethernet_create_nm_query(ethernet);
    } else {
        amxb_subscription_delete(&(ethernet->netdev_status_subscription));
        mod_vlan_execute_function("destroy-vlan", ethernet);
        ethernet_close_nm_query(ethernet);
        dm_ethernet_update_status(ethernet, STATUS_DOWN);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_llayer_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = ethernet_get_dm();
    ethernet_info_t* ethernet = NULL;
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    bool enabled = amxd_object_get_value(bool, object, "Enable", NULL);

    SAH_TRACEZ_INFO(ME, "LowerLayer changed from \"%s\" to \"%s\"",
                    GETP_CHAR(data, "parameters.LowerLayers.from"),
                    GETP_CHAR(data, "parameters.LowerLayers.to"));

    if(enabled) {
        when_null_trace(object, exit, ERROR, "could not get object");
        when_null_trace(object->priv, exit, ERROR, "object has no private data");
        ethernet = (ethernet_info_t*) object->priv;

        mod_vlan_execute_function("destroy-vlan", ethernet);
        ethernet_close_nm_query(ethernet);
        ethernet_create_nm_query(ethernet);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_vlanid_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = ethernet_get_dm();
    ethernet_info_t* ethernet = NULL;
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    bool enabled = amxd_object_get_value(bool, object, "Enable", NULL);

    SAH_TRACEZ_INFO(ME, "VLANID changed from \"%d\" to \"%d\"",
                    GETP_UINT32(data, "parameters.VLANID.from"),
                    GETP_UINT32(data, "parameters.VLANID.to"));

    if(enabled) {
        when_null_trace(object, exit, ERROR, "could not get object");
        when_null_trace(object->priv, exit, ERROR, "object has no private data");
        ethernet = (ethernet_info_t*) object->priv;

        mod_vlan_execute_function("destroy-vlan", ethernet);
        ethernet_close_nm_query(ethernet);
        ethernet_create_nm_query(ethernet);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_vlanprio_changed(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = ethernet_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    bool enabled = amxd_object_get_value(bool, object, "Enable", NULL);

    SAH_TRACEZ_INFO(ME, "VLANPriority changed from \"%d\" to \"%d\"",
                    GETP_UINT32(data, "parameters.VLANPriority.from"),
                    GETP_UINT32(data, "parameters.VLANPriority.to"));

    if(enabled) {
        ethernet_info_t* ethernet = NULL;
        when_null_trace(object, exit, ERROR, "could not get object");
        when_null_trace(object->priv, exit, ERROR, "object has no private data");
        ethernet = (ethernet_info_t*) object->priv;

        mod_vlan_execute_function("set-vlan-priority", ethernet);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_link_deleted(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t spath;
    amxd_object_t* link_templ_obj = amxd_dm_signal_get_object(ethernet_get_dm(), data);

    amxc_string_init(&spath, 0);

    amxc_string_setf(&spath, ".^.VLANTermination.[LowerLayers == 'Device.%s%d']", GETP_CHAR(data, "path"), GETP_INT32(data, "index"));
    amxd_object_for_all(link_templ_obj, amxc_string_get(&spath, 0), _ethernet_vlanterm_disable_cb, NULL);

    amxc_string_clean(&spath);
    SAH_TRACEZ_OUT(ME);
}

void _ethernet_link_mac_changed(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = ethernet_get_dm();
    ethernet_info_t* ethernet = NULL;
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    char* mac_addr = amxd_object_get_value(cstring_t, object, "MACAddress", NULL);

    when_null_trace(object, exit, ERROR, "could not get object");
    when_null_trace(object->priv, exit, ERROR, "object has no private data");
    ethernet = (ethernet_info_t*) object->priv;
    set_mac_address(ethernet, mac_addr);

exit:
    free(mac_addr);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_set_interface_config(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* object = amxd_dm_signal_get_object(ethernet_get_dm(), data);
    ethernet_info_t* ethernet = NULL;

    when_null_trace(object, exit, ERROR, "Could not get object");
    ethernet = (ethernet_info_t*) object->priv;
    when_null_trace(ethernet, exit, ERROR, "Object private data is null");

    rv = dm_ethernet_ethtool_set(ethernet);
    when_failed_trace(rv, exit, ERROR, "Failed to write bitrate and duplex mode");

exit:
    SAH_TRACEZ_OUT(ME);
}

void _ethernet_set_mtu(UNUSED const char* const sig_name,
                       const amxc_var_t* const data,
                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* object = amxd_dm_signal_get_object(ethernet_get_dm(), data);
    ethernet_info_t* ethernet = NULL;

    when_null_trace(object, exit, ERROR, "Could not get object");
    ethernet = (ethernet_info_t*) object->priv;

    rv = mod_ethernet_set_mtu(ethernet);
    when_failed_trace(rv, exit, ERROR, "Failed to set MTU for '%s", ethernet->netdev_intf);

exit:
    SAH_TRACEZ_OUT(ME);
}

void _ethernet_set_interface_led(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* object = amxd_dm_signal_get_object(ethernet_get_dm(), data);
    ethernet_info_t* ethernet = NULL;

    when_null_trace(object, exit, ERROR, "Could not get object");
    ethernet = (ethernet_info_t*) object->priv;
    when_null_trace(ethernet, exit, ERROR, "Object private data is null");

    rv = mod_ethernet_set_led(ethernet);
    when_failed_trace(rv, exit, ERROR, "Failed to write led mode");

exit:
    SAH_TRACEZ_OUT(ME);
}

static amxd_status_t rmonstats_set_name(ethernet_rmonstats_info_t* rmonstats_info, const char* name) {
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_null_trace(rmonstats_info, exit, ERROR, "Object private data is null");
    when_null_trace(rmonstats_info->object, exit, ERROR, "private data has no object reference");

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, rmonstats_info->object);

    amxd_trans_set_value(cstring_t,
                         &trans,
                         "Name",
                         name);
    rv = amxd_trans_apply(&trans, ethernet_get_dm());

exit:
    amxd_trans_clean(&trans);
    return rv;
}

static amxd_status_t rmonstats_set_vlanid(ethernet_rmonstats_info_t* rmonstats_info, ethernet_info_t* ethernet_info, bool set) {
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t trans;
    uint32_t vlanid = 0;

    amxd_trans_init(&trans);

    when_null_trace(rmonstats_info, exit, ERROR, "Object private data is null");
    when_null_trace(rmonstats_info->object, exit, ERROR, "private data has no object reference");

    when_null_trace(ethernet_info, exit, ERROR, "Object private data is null");
    when_null_trace(ethernet_info->object, exit, ERROR, "private data has no object reference");

    vlanid = set ? amxd_object_get_value(uint32_t, ethernet_info->object, "VLANID", NULL) : 0;

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, rmonstats_info->object);

    amxd_trans_set_value(uint32_t, &trans, "VLANID", vlanid);
    rv = amxd_trans_apply(&trans, ethernet_get_dm());

exit:
    amxd_trans_clean(&trans);
    return rv;
}

static amxd_status_t rmonstats_set_status(ethernet_rmonstats_info_t* rmonstats_info) {
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_null_trace(rmonstats_info, exit, ERROR, "Object private data is null");
    when_null_trace(rmonstats_info->object, exit, ERROR, "private data has no object reference");

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, rmonstats_info->object);

    amxd_trans_set_value(cstring_t,
                         &trans,
                         "Status",
                         (rmonstats_info->status < RMONSTATS_NUM_STATUS) ? rmonstats_states_table[rmonstats_info->status].str_status:"");
    rv = amxd_trans_apply(&trans, ethernet_get_dm());

exit:
    amxd_trans_clean(&trans);
    return rv;
}

static void ethernet_rmonstats_intf_changed(amxd_object_t* rmonstats_obj, const char* intrf) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_object_t* link_obj = NULL;
    ethernet_rmonstats_info_t* rmonstats_info = NULL;
    ethernet_info_t* ethernet_info = NULL;
    amxc_string_t intrf_str;

    amxc_string_init(&intrf_str, 0);
    when_null_trace(rmonstats_obj, exit, ERROR, "Could not get object");
    rmonstats_info = (ethernet_rmonstats_info_t*) rmonstats_obj->priv;
    when_null_trace(rmonstats_info, exit, ERROR, "Object private data is null");

    if(str_empty(intrf)) {
        SAH_TRACEZ_ERROR(ME, "No ethernet interface specified");
        rmonstats_info->status = rmonstats_info->enable ? RMONSTATS_ERR_MISCONFIGURED : RMONSTATS_DISABLED;
        goto set_status;
    }

    amxc_string_setf(&intrf_str, "%s", intrf);
    amxc_string_replace(&intrf_str, "Device.", "", UINT32_MAX);
    link_obj = amxd_dm_findf(ethernet_get_dm(), "%s", amxc_string_get(&intrf_str, 0));

    free(rmonstats_info->interface);
    rmonstats_info->interface = strdup(amxc_string_get(&intrf_str, 0));

    if((link_obj == NULL) || (link_obj->priv == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Could not get Link object reference");
        rmonstats_info->status = RMONSTATS_ERR_MISCONFIGURED;
    } else {
        ethernet_info = (ethernet_info_t*) link_obj->priv;
        rmonstats_info->status = rmonstats_info->enable ? RMONSTATS_ENABLED : RMONSTATS_DISABLED;

        if(ethernet_info->type == ETHERNET_VLANTERMINATION) {
            rmonstats_set_vlanid(rmonstats_info, ethernet_info, true);
        } else {
            rmonstats_set_vlanid(rmonstats_info, ethernet_info, false);
        }

        if(ethernet_info->type == ETHERNET_INTERFACE) {
            ethernet_switch_rmon_stats_init(rmonstats_obj, rmonstats_info->interface);
        }
    }

set_status:
    rv = rmonstats_set_status(rmonstats_info);
    when_failed_trace(rv, exit, ERROR, "Could not apply transaction on status for Ethernet.RMONStats.%d", rmonstats_obj->index);
exit:
    amxc_string_clean(&intrf_str);
    SAH_TRACEZ_OUT(ME);
}

void _ethernet_rmonstats_added(UNUSED const char* const sig_name,
                               const amxc_var_t* const data,
                               UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = ethernet_get_dm();
    ethernet_rmonstats_info_t* rmonstats_info = NULL;
    amxd_object_t* rmonstats_templ_obj = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* rmonstats_obj = NULL;
    bool enable = GETP_BOOL(data, "parameters.Enable");
    const char* alias = GETP_CHAR(data, "parameters.Alias");
    const char* name = GETP_CHAR(data, "parameters.Name");
    const char* interface = GETP_CHAR(data, "parameters.Interface");

    when_null_trace(rmonstats_templ_obj, exit, ERROR, "Could not get template object");
    rmonstats_obj = amxd_object_get_instance(rmonstats_templ_obj, NULL, GET_UINT32(data, "index"));
    when_null_trace(rmonstats_obj, exit, ERROR, "Could not get the added instance");

    when_not_null(rmonstats_obj->priv, exit);

    rv = rmonstats_info_new(&rmonstats_info, enable);
    when_failed(rv, exit);

    rmonstats_info->object = rmonstats_obj;
    rmonstats_obj->priv = rmonstats_info;
    if(!rmonstats_info->enable) {
        rmonstats_set_status(rmonstats_info);
    }

    if(str_empty(name)) {
        when_str_empty_trace(alias, exit, ERROR, "No alias could be found");
        rmonstats_set_name(rmonstats_info, alias);
    }

    ethernet_rmonstats_intf_changed(rmonstats_obj, interface);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ethernet_rmonstats_intf_changed(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* rmonstats_obj = amxd_dm_signal_get_object(ethernet_get_dm(), data);
    const char* intrf = GETP_CHAR(data, "parameters.Interface.to");

    ethernet_rmonstats_intf_changed(rmonstats_obj, intrf);

    SAH_TRACEZ_OUT(ME);
}

void _ethernet_rmonstats_enable(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_object_t* rmonstats_obj = amxd_dm_signal_get_object(ethernet_get_dm(), data);
    amxd_object_t* link_obj = NULL;
    ethernet_rmonstats_info_t* rmonstats_info = NULL;
    bool enable = GETP_BOOL(data, "parameters.Enable.to");

    when_null_trace(rmonstats_obj, exit, ERROR, "Could not get object");
    rmonstats_info = (ethernet_rmonstats_info_t*) rmonstats_obj->priv;
    when_null_trace(rmonstats_info, exit, ERROR, "Object private data is null");

    rmonstats_info->enable = enable;

    link_obj = amxd_dm_findf(ethernet_get_dm(), "%s", rmonstats_info->interface);

    if(enable) {
        if(link_obj == NULL) {
            rmonstats_info->status = RMONSTATS_ERR_MISCONFIGURED;
        } else {
            rmonstats_info->status = RMONSTATS_ENABLED;
        }
    } else {
        rmonstats_info->status = RMONSTATS_DISABLED;
    }

    rv = rmonstats_set_status(rmonstats_info);
    when_failed_trace(rv, exit, ERROR, "Could not apply transaction on status for %s", GET_CHAR(data, "path"));

exit:
    SAH_TRACEZ_OUT(ME);
}

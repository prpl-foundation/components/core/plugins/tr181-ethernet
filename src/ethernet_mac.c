/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ethernet.h"
#include "ethernet_utils.h"
#include "ethernet_soc_utils.h"
#include "ethernet_mac.h"

#define ME "mac"

int set_mac_address(ethernet_info_t* ethernet, const char* mac_addr) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* llayers = NULL;
    amxc_var_t data;
    amxc_var_t ret;
    const char* ctrl = NULL;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_null_trace(ethernet, exit, ERROR, "Given ethernet pointer is NULL");
    when_null_trace(ethernet->object, exit, ERROR, "object can not be NULL, no valid path");
    when_str_empty_trace(ethernet->netdev_intf, exit, ERROR, "Given netdev intf is empty");
    when_str_empty_trace(mac_addr, exit, ERROR, "Given mac address is empty");

    llayers = amxd_object_get_value(cstring_t, ethernet->object, "LowerLayers", NULL);
    when_str_empty_status(llayers, exit, rv = 0);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "intf_name", ethernet->netdev_intf);
    amxc_var_add_key(cstring_t, &data, "mac_address", mac_addr);

    ctrl = mod_ctrl_get(ethernet->object, ETH_LLA_CTRL);
    SAH_TRACEZ_INFO(ME, "Calling mac-address-set implementation (controller = '%s')", ctrl);
    rv = amxm_execute_function(ctrl, MOD_ETH_LLA_CTRL, "mac-address-set", &data, &ret);
    when_failed_trace(rv, exit, ERROR, "Failed to call mac-address-set, return '%d'", rv);

exit:
    if(rv != 0) {
        dm_ethernet_update_status(ethernet, STATUS_ERROR);
    }
    free(llayers);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int get_mac_address(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_trans_t trans;
    amxc_var_t data;
    amxc_var_t ret;
    const char* ctrl = NULL;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxd_trans_init(&trans);

    when_null_trace(ethernet, exit, ERROR, "No ethernet info provided");
    when_null_trace(ethernet->object, exit, ERROR, "object can not be NULL, no valid path");
    when_str_empty_trace(ethernet->netdev_intf, exit, ERROR, "Interface name can not be empty");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "intf_name", ethernet->netdev_intf);

    ctrl = mod_ctrl_get(ethernet->object, ETH_LLA_CTRL);
    SAH_TRACEZ_INFO(ME, "Calling mac-address-get implementation (controller = '%s')", ctrl);
    rv = amxm_execute_function(ctrl, MOD_ETH_LLA_CTRL, "mac-address-get", &data, &ret);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to call mac-address-get, return '%d'", rv);
        dm_ethernet_update_status(ethernet, STATUS_ERROR);
        goto exit;
    }

    amxd_trans_select_object(&trans, ethernet->object);
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", GET_CHAR(&ret, "mac_address"));
    rv = amxd_trans_apply(&trans, ethernet_get_dm());

exit:
    amxd_trans_clean(&trans);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int handle_mac_address(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* mac_addr = amxd_object_get_value(cstring_t, ethernet->object, "MACAddress", NULL);
    if((mac_addr == NULL) || (*mac_addr == 0)) {
        rv = get_mac_address(ethernet);
    } else {
        rv = set_mac_address(ethernet, mac_addr);
    }

    free(mac_addr);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int set_ethernet_enabled(const char* netdev_intf, amxd_object_t* obj, bool enable) {
    int rv = -1;
    amxc_var_t data;
    amxc_var_t ret;
    const char* ctrl = mod_ctrl_get(obj, ETH_LLA_CTRL);

    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_str_empty_trace(netdev_intf, exit, ERROR, "Interface name can not be empty");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "intf_name", netdev_intf);
    amxc_var_add_key(bool, &data, "enable", enable);

    SAH_TRACEZ_INFO(ME, "Calling ethernet-enable-set implementation (controller = '%s')", ctrl);
    rv = amxm_execute_function(ctrl, MOD_ETH_LLA_CTRL, "ethernet-enable-set", &data, &ret);
    when_failed_trace(rv, exit, ERROR, "Failed to call ethernet-enable-set, return '%d'", rv);

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return rv;
}

/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ethernet.h"
#include "ethernet_utils.h"
#include "ethernet_soc_utils.h"
#include "ethernet_actions.h"

#define ME "actions"

amxd_status_t _check_is_empty_or_in(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;

    rv = amxd_action_param_check_is_in(object, param, reason, args, retval, priv);

    if(rv != amxd_status_ok) {
        char* input = amxc_var_dyncast(cstring_t, args);
        if((input == NULL) || (*input == 0)) {
            rv = amxd_status_ok;
        }
        free(input);
    }

    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _lastchange_on_read(amxd_object_t* const object,
                                  UNUSED amxd_param_t* const param,
                                  amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  amxc_var_t* const retval,
                                  UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    ethernet_info_t* ethernet = NULL;
    uint32_t since_change = 0;
    uint32_t uptime = 0;

    if(reason != action_param_read) {
        SAH_TRACEZ_NOTICE(ME, "wrong reason, expected action_param_read(%d) got %d", action_param_read, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    // retval needs to be set, in case of error lastchange will be 0
    amxc_var_set_uint32_t(retval, 0);
    when_null_trace(object, exit, ERROR, "object can not be NULL");
    when_null(object->priv, exit);

    ethernet = (ethernet_info_t*) object->priv;

    uptime = get_system_uptime();
    when_true_trace(uptime < ethernet->last_change, exit, ERROR, "UpTime is smaller than last change");
    since_change = uptime - ethernet->last_change;
    when_failed_trace(amxc_var_set_uint32_t(retval, since_change),
                      exit, ERROR, "failed to set parameter lastchange");

    rv = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _ethernet_instance_removed(amxd_object_t* object,
                                         UNUSED amxd_param_t* param,
                                         amxd_action_t reason,
                                         UNUSED const amxc_var_t* const args,
                                         UNUSED amxc_var_t* const retval,
                                         UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    ethernet_info_t* ethernet = NULL;
    amxd_status_t rv = amxd_status_invalid_function_argument;

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "object can not be null, invalid argument");

    SAH_TRACEZ_INFO(ME, "Removing: %s", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    if(object->type != amxd_object_instance) {
        SAH_TRACEZ_INFO(ME, "Skipping template object, only remove instances");
        rv = amxd_status_ok;
        goto exit;
    }

    when_null(object->priv, exit);
    ethernet = (ethernet_info_t*) object->priv;

    rv = ethernet_info_clean(&ethernet);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _ethernet_vlanterm_removed(amxd_object_t* object,
                                         UNUSED amxd_param_t* param,
                                         amxd_action_t reason,
                                         UNUSED const amxc_var_t* const args,
                                         UNUSED amxc_var_t* const retval,
                                         UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    ethernet_info_t* ethernet = NULL;
    amxd_status_t rv = amxd_status_invalid_function_argument;
    bool enabled = amxd_object_get_value(bool, object, "Enable", NULL);

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "object can not be null, invalid argument");

    SAH_TRACEZ_INFO(ME, "Removing: %s", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    if(object->type != amxd_object_instance) {
        SAH_TRACEZ_INFO(ME, "Skipping template object, only remove instances");
        rv = amxd_status_ok;
        goto exit;
    }

    when_null(object->priv, exit);
    ethernet = (ethernet_info_t*) object->priv;

    if(enabled) {
        mod_vlan_execute_function("destroy-vlan", ethernet);
    }

    rv = ethernet_info_clean(&ethernet);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _ethernet_check_interface_upstream(amxd_object_t* object,
                                                 amxd_param_t* param,
                                                 amxd_action_t reason,
                                                 const amxc_var_t* const args,
                                                 amxc_var_t* const retval,
                                                 void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_object_t* interface = NULL;
    bool interface_upstream = false;
    bool interface_upstream_old = false;

    if(reason != action_param_validate) {
        rv = amxd_status_invalid_action;
        goto exit;
    }

    // checks if value can be converted to parameter type.
    rv = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(rv, exit);

    interface_upstream = amxc_var_dyncast(bool, args);
    interface_upstream_old = amxc_var_dyncast(bool, &param->value);

    if(interface_upstream && !interface_upstream_old) {
        interface = amxd_object_findf(object, "^.[Upstream == true]");
        when_not_null_status(interface, exit, rv = amxd_status_invalid_value);
        SAH_TRACEZ_INFO(ME, "%s is now configured in upstream", object->name);
        rv = amxd_status_ok;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _check_overlapping_vlantermination(UNUSED amxd_object_t* const object,
                                                 amxd_param_t* const param,
                                                 UNUSED amxd_action_t reason,
                                                 UNUSED const amxc_var_t* const args,
                                                 UNUSED amxc_var_t* const retval,
                                                 UNUSED void* priv) {

    amxd_status_t ret = amxd_status_unknown_error;
    amxd_object_t* vlanterm_templ_obj = NULL;
    bool enable = true;
    const char* name = NULL;
    const char* ll = NULL;
    uint32_t vlanid = 0;

    when_true_status(reason != action_param_validate, exit, ret = amxd_status_function_not_implemented);
    when_null_trace(object, exit, ERROR, "Ethernet object is not given");
    vlanterm_templ_obj = amxd_object_get_parent(object);
    when_null_trace(vlanterm_templ_obj, exit, ERROR, "Cannot get vlantermination template object");

    if(strcmp(amxd_param_get_name(param), "VLANID") == 0) {
        vlanid = GET_UINT32(args, NULL);
        enable = GET_BOOL(amxd_object_get_param_value(object, "Enable"), NULL);
        ll = GET_CHAR(amxd_object_get_param_value(object, "LowerLayers"), NULL);
    } else if(strcmp(amxd_param_get_name(param), "LowerLayers") == 0) {
        vlanid = GET_UINT32(amxd_object_get_param_value(object, "VLANID"), NULL);
        enable = GET_BOOL(amxd_object_get_param_value(object, "Enable"), NULL);
        ll = GET_CHAR(args, NULL);
    } else {
        vlanid = GET_UINT32(amxd_object_get_param_value(object, "VLANID"), NULL);
        enable = GET_BOOL(args, NULL);
        ll = GET_CHAR(amxd_object_get_param_value(object, "LowerLayers"), NULL);
    }
    name = GET_CHAR(amxd_object_get_param_value(object, "Name"), NULL);

    if(!enable || str_empty(ll)) {
        ret = amxd_status_ok;
        goto exit;
    }

    // If the user adds an enabled instance it cannot have the same name or the same vlanid and the same lower layer as another enbaled instance.
    ret = amxd_status_invalid_value;
    amxd_object_for_each(instance, vlan_it, vlanterm_templ_obj) {
        amxd_object_t* vlan_termination = amxc_container_of(vlan_it, amxd_object_t, it);
        if(vlan_termination != object) {
            uint32_t vlanid_current = GET_UINT32(amxd_object_get_param_value(vlan_termination, "VLANID"), NULL);
            bool enable_current = GET_BOOL(amxd_object_get_param_value(vlan_termination, "Enable"), NULL);
            const char* name_current = GET_CHAR(amxd_object_get_param_value(vlan_termination, "Name"), NULL);
            const char* ll_current = GET_CHAR(amxd_object_get_param_value(vlan_termination, "LowerLayers"), NULL);
            if(!enable_current || str_empty(ll_current)) {
                continue;
            }
            when_true_trace((strcmp(name, name_current) == 0) || ((vlanid == vlanid_current) && (strcmp(ll, ll_current) == 0)), exit, WARNING, "Not allowing vlantermination to start, overlapping parameters with other instances");
        }
    }
    ret = amxd_status_ok;
exit:
    return ret;
}

amxd_status_t _rmonstats_on_read(amxd_object_t* const object,
                                 UNUSED amxd_param_t* const param,
                                 amxd_action_t reason,
                                 UNUSED const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    ethernet_rmonstats_info_t* rmonstats_info = NULL;
    ethernet_info_t* ethernet_info = NULL;
    amxc_var_t data;
    amxd_object_t* link_object = NULL;
    const char* ctrl = mod_ctrl_get(NULL, ETH_RMONSTATS_CTRL);

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    status = amxd_action_object_read(object, param, reason, args, retval, priv);

    when_true(status != amxd_status_ok &&
              status != amxd_status_parameter_not_found,
              exit);

    when_str_empty_trace(ctrl, exit, ERROR, "No module was installed");

    when_null_trace(object, exit, ERROR, "Object can not be NULL");
    when_null_trace(object->priv, exit, ERROR, "No private structure yet available");
    rmonstats_info = (ethernet_rmonstats_info_t*) object->priv;
    when_null_trace(rmonstats_info, exit, ERROR, "No priv structure available");
    when_str_empty_trace(rmonstats_info->interface, exit, ERROR, "No interface string specified");
    when_true_trace(rmonstats_info->status != RMONSTATS_ENABLED, exit, ERROR, "RMONStats missconfigured, not calling the module");

    link_object = amxd_dm_findf(ethernet_get_dm(), "%s", rmonstats_info->interface);
    when_null_trace(link_object, exit, ERROR, "Could not get link object");
    when_null_trace(link_object->priv, exit, ERROR, "No private structure yet available");
    ethernet_info = (ethernet_info_t*) link_object->priv;
    when_null_trace(ethernet_info, exit, ERROR, "No Link priv structure found in Link object");

    amxc_var_add_key(cstring_t, &data, "network_intf", str_empty(ethernet_info->netdev_intf) ? "" : ethernet_info->netdev_intf);

    if(ethernet_info->type == ETHERNET_INTERFACE) {
        ctrl = mod_ctrl_get(ethernet_info->object, ETH_RMONSTATS_CTRL);
        when_str_empty_trace(ctrl, exit, ERROR, "Failed to get ctrl");
    }

    status = amxm_execute_function(ctrl, MOD_ETH_RMONSTATS_CTRL, "get-stats-rmonstats", &data, retval);
    when_failed_trace(status, exit, ERROR, "Failed to call get-stats-rmonstats, return '%d'", status);

exit:
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _rmonstats_instance_removed(amxd_object_t* const object,
                                          UNUSED amxd_param_t* const param,
                                          amxd_action_t reason,
                                          UNUSED const amxc_var_t* const args,
                                          UNUSED amxc_var_t* const retval,
                                          UNUSED void* priv) {

    SAH_TRACEZ_IN(ME);
    ethernet_rmonstats_info_t* rmonstats_info = NULL;
    amxd_status_t rv = amxd_status_invalid_function_argument;

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "object can not be null, invalid argument");

    SAH_TRACEZ_INFO(ME, "Removing: %s", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    if(object->type != amxd_object_instance) {
        SAH_TRACEZ_INFO(ME, "Skipping template object, only remove instances");
        rv = amxd_status_ok;
        goto exit;
    }

    when_null(object->priv, exit);
    rmonstats_info = (ethernet_rmonstats_info_t*) object->priv;

    rv = rmonstats_info_clean(&rmonstats_info);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

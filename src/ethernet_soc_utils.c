/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ethernet.h"
#include "ethernet_utils.h"
#include "ethernet_soc_utils.h"

#define ME "utils"

static const char* ethernet_get_vlan_ctrl(void) {
    SAH_TRACEZ_IN(ME);
    const char* vlan_ctrl_name = NULL;

    if(ethernet_vlan_mods_are_loaded()) {
        vlan_ctrl_name = mod_ctrl_get(NULL, VLAN_CTRL);
    } else {
        vlan_ctrl_name = DUMMY_VLAN_MOD_NAME;
    }

    SAH_TRACEZ_OUT(ME);
    return vlan_ctrl_name;
}

static int mod_vlan_build_data_struct(ethernet_info_t* ethernet, amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* link_name = NULL;
    amxc_var_t* extra_params = NULL;
    amxc_var_t* vlanterm_params = NULL;

    when_null_trace(ethernet, exit, ERROR, "object can not be NULL, no valid path");
    when_null_trace(ethernet->object, exit, ERROR, "object can not be NULL, no valid path");
    when_null_trace(ethernet->netdev_intf, exit, ERROR, "Netdev interface name must be set");

    link_name = str_empty(ethernet->link_name) ? "" : ethernet->link_name;

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    extra_params = amxc_var_add_key(amxc_htable_t, data, "ethernet", NULL);
    vlanterm_params = amxc_var_add_key(amxc_htable_t, extra_params, "vlanterm_params", NULL);
    rv = amxd_object_get_params(ethernet->object, vlanterm_params, amxd_dm_access_protected);

    // Add mandatory arguments
    amxc_var_add_key(cstring_t, data, "port_name", link_name);
    amxc_var_add_key(uint32_t, data, "egress_priority", GETP_UINT32(vlanterm_params, "VLANPriority"));
    amxc_var_add_key(cstring_t, data, "vlanport_name", ethernet->netdev_intf);
    amxc_var_add_key(int32_t, data, "vlanid", GETP_UINT32(vlanterm_params, "VLANID"));

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

const char* mod_ctrl_get(amxd_object_t* obj, const char* ctrl_param) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ethernet_obj = amxd_dm_findf(ethernet_get_dm(), "Ethernet");
    const amxc_var_t* ctrl_arg = NULL;
    const char* ctrl_name = NULL;

    // If no object is given, use the default
    if(obj == NULL) {
        obj = ethernet_obj;
    }
    ctrl_arg = amxd_object_get_param_value(obj, ctrl_param);

    // If no controller value is found, try the default
    if(ctrl_arg == NULL) {
        ctrl_arg = amxd_object_get_param_value(ethernet_obj, ctrl_param);
    }

    ctrl_name = GET_CHAR(ctrl_arg, NULL);

    SAH_TRACEZ_OUT(ME);
    return ctrl_name;
}

int mod_vlan_execute_function(const char* function, ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t data;
    amxc_var_t ret;
    const char* vlan_ctrl = NULL;
    int rv = -1;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_str_empty_trace(function, exit, ERROR, "No function provided to execute");
    when_null_trace(ethernet, exit, ERROR, "No ethernet info available");
    when_null_trace(ethernet->object, exit, ERROR, "Can not execute %s, no object given", function);

    vlan_ctrl = ethernet_get_vlan_ctrl();
    when_failed(mod_vlan_build_data_struct(ethernet, &data), exit);
    SAH_TRACEZ_INFO(ME, "%s -> call implementation (controller = '%s')", function, vlan_ctrl);
    if(amxm_execute_function(vlan_ctrl, MOD_VLAN_CTRL, function, &data, &ret) != 0) {
        SAH_TRACEZ_ERROR(ME, "function %s failed", function);
        dm_ethernet_update_status(ethernet, STATUS_ERROR);
    }

    rv = 0;

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_ethernet_set_mtu(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxo_parser_t* parser = ethernet_get_parser();
    const char* prefix = GETP_CHAR(&parser->config, "prefix_");
    const char* mtu_mode_str = NULL;
    amxc_string_t tmp_str;

    amxc_string_init(&tmp_str, 0);

    when_null_trace(ethernet, exit, ERROR, "No ethernet info provided");
    when_str_empty_trace(ethernet->netdev_intf, exit, ERROR, "Netdev interface name must be set");
    when_null_trace(ethernet->object, exit, ERROR, "object can not be NULL, no valid path");

    amxc_string_setf(&tmp_str, "%sMTUMode", prefix);

    mtu_mode_str = GET_CHAR(amxd_object_get_param_value(ethernet->object,
                                                        amxc_string_get(&tmp_str, 0)),
                            NULL);
    if(!str_empty(mtu_mode_str) && (strcmp(mtu_mode_str, "manual") == 0)) {
        const char* ctrl = mod_ctrl_get(ethernet->object, ETH_LLA_CTRL);
        const amxc_var_t* mtu = NULL;
        amxc_var_t args;
        amxc_var_t ret;

        amxc_var_init(&args);
        amxc_var_init(&ret);

        amxc_string_setf(&tmp_str, "%sMTU", prefix);
        mtu = amxd_object_get_param_value(ethernet->object, amxc_string_get(&tmp_str, 0));
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "intf_name", ethernet->netdev_intf);
        amxc_var_add_key(uint32_t, &args, "mtu_size", GET_UINT32(mtu, NULL));

        SAH_TRACEZ_INFO(ME, "Calling mtu-set implementation (controller = '%s')", ctrl);
        rv = amxm_execute_function(ctrl, MOD_ETH_LLA_CTRL, "mtu-set", &args, &ret);

        amxc_var_clean(&args);
        amxc_var_clean(&ret);
        when_failed_trace(rv, exit, INFO, "Failed to call mtu-set, return '%d'", rv)
    }
    rv = 0;

exit:
    amxc_string_clean(&tmp_str);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static const char* ethernet_get_led_ctrl(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    const char* led_ctrl_name = NULL;

    if(ethernet_led_mods_are_loaded()) {
        led_ctrl_name = mod_ctrl_get(ethernet->object, ETH_LED_CTRL);
    } else {
        led_ctrl_name = DUMMY_LED_MOD_NAME;
    }

    SAH_TRACEZ_OUT(ME);
    return led_ctrl_name;
}

static amxd_status_t ethernet_led_update_status(ethernet_info_t* ethernet, const char* status) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = ethernet_get_dm();
    amxd_trans_t trans;
    const char* new_status = NULL;
    amxd_trans_init(&trans);

    when_null_trace(ethernet, exit, ERROR, "No ethernet info provided");
    when_null_trace(ethernet->led_object, exit, ERROR, "LED object can not be NULL, can not change state");
    when_str_empty(status, exit);

    if(strcmp(status, STATUS_NOT_PRESENT) == 0) {
        new_status = STATUS_NOT_PRESENT;
    } else if(strcmp(status, STATUS_ENABLED) == 0) {
        new_status = STATUS_ENABLED;
    } else if(strcmp(status, STATUS_ERROR) == 0) {
        new_status = STATUS_ERROR;
    } else if(strcmp(status, STATUS_DISABLED) == 0) {
        new_status = STATUS_DISABLED;
    } else {
        SAH_TRACEZ_ERROR(ME, "Setting the LED.Status '%s' is not supported", status);
        new_status = STATUS_UNKNOWN;
    }

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, ethernet->led_object);
    amxd_trans_set_value(cstring_t, &trans, "Status", new_status);
    rv = amxd_trans_apply(&trans, dm);

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_ethernet_set_led(ethernet_info_t* ethernet) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* led_ctrl = NULL;
    const amxc_var_t* led_enabled = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(ethernet, exit, ERROR, "No ethernet info provided");
    when_null_trace(ethernet->object, exit, ERROR, "Object can not be NULL");
    when_null_trace(ethernet->netdev_intf, exit, ERROR, "Netdev interface name must be set");
    when_null_trace(ethernet->led_object, exit, ERROR, "LED object can not be NULL");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    led_enabled = amxd_object_get_param_value(ethernet->led_object, "Enable");
    when_null_trace(led_enabled, exit, ERROR, "Could not get the LED object 'Enable' parameter");

    amxc_var_add_key(cstring_t, &args, "intf_name", ethernet->netdev_intf);
    amxc_var_add_key(bool, &args, "led_enabled", GET_BOOL(led_enabled, NULL));

    led_ctrl = ethernet_get_led_ctrl(ethernet);
    if(str_empty(led_ctrl)) {
        SAH_TRACEZ_INFO(ME, "No LEDController defined (interface = '%s')", ethernet->netdev_intf);
        rv = ethernet_led_update_status(ethernet, STATUS_NOT_PRESENT);
        when_failed_trace(rv, exit, ERROR, "Failed to set LED.Status NotPresent, return '%d'", rv);
    } else {
        SAH_TRACEZ_INFO(ME, "Calling port-led-set implementation (controller = '%s')", led_ctrl);
        rv = amxm_execute_function(led_ctrl, MOD_ETH_LED_CTRL, "port-led-set", &args, &ret);
        when_failed_trace(rv, exit, ERROR, "Failed to call port-led-set, return '%d'", rv);
        rv = ethernet_led_update_status(ethernet, GET_BOOL(led_enabled, NULL) ? STATUS_ENABLED : STATUS_DISABLED);
        when_failed_trace(rv, exit, ERROR, "Failed to set LED.Status, return '%d'", rv);
    }

exit:
    if(rv != 0) {
        ethernet_led_update_status(ethernet, STATUS_ERROR);
    }
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

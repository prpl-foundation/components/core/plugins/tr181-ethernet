/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ethernet.h"
#include "ethernet_mac.h"

#define ME "ethernet"

static ethernet_app_t ethernet;

static int dm_mngr_load_controllers(const char* supported_cntrlrs_param, const char* mod_dir) {
    SAH_TRACEZ_IN(ME);
    int rv = 0;
    amxd_dm_t* dm = ethernet_get_dm();
    amxd_object_t* ethernet_obj = amxd_dm_findf(dm, "Ethernet");
    const amxc_var_t* controllers = amxd_object_get_param_value(ethernet_obj,
                                                                supported_cntrlrs_param);
    amxc_var_t lcontrollers;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "%s/%s.so", mod_dir, name);
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)", name, amxc_string_get(&mod_path, 0));
        rv = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' %s", name, rv ? "failed" : "successful");
        when_failed_trace(rv, exit, WARNING, "Not continuing with loading modules");
    }

exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int dm_mngr_load_dummy(const char* mod_dir, const char* name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;

    amxc_string_init(&mod_path, 0);

    amxc_string_setf(&mod_path, "%s/%s.so", mod_dir, name);
    SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)", name, amxc_string_get(&mod_path, 0));
    rv = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));

    amxc_string_clean(&mod_path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int ethernet_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    SAH_TRACEZ_INFO(ME, "Ethernet manager start");
    const char* mod_dir = GETP_CHAR(&parser->config, "ethernet-manager.mod-dir");
    const char* external_mod_dir = GETP_CHAR(&parser->config, "ethernet-manager.external-mod-dir");

    ethernet.dm = dm;
    ethernet.parser = parser;
    ethernet.netdev_ctx = amxb_be_who_has("NetDev.");
    when_null_trace(ethernet.netdev_ctx, exit, ERROR, "failed to get bus context for NetDev");

    when_false_trace(netmodel_initialize(), exit, ERROR, "Failed to initialize libnetmodel");

    // Defaults or saved odl will be loaded here instead of in ethernet-manager.odl
    // This makes sure that the events for the defaults are only called when the plugin is started
    // and not while it is waiting on a required object from an other plugin (NetDev.)
    rv = amxo_parser_parse_string(parser, "?include '${odl.directory}/${name}.odl':'${odl.dm-defaults}'; include '${odl.mac-defaults}';", amxd_dm_get_root(dm));
    when_failed_trace(rv, exit, ERROR, "Failed to load defaults or persistency");

    rv = dm_mngr_load_controllers("SupportedControllers", mod_dir);
    when_failed_trace(rv, exit, ERROR, "Failed to load all SupportedControllers");
    rv = dm_mngr_load_controllers("SupportedVLANControllers", external_mod_dir);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load all SupportedVLANControllers");
        ethernet.vlan_mods_are_loaded = false;
        rv = dm_mngr_load_dummy(mod_dir, DUMMY_VLAN_MOD_NAME);
        when_failed_trace(rv, exit, ERROR, "Failed to load dummy VLANController");
    } else {
        ethernet.vlan_mods_are_loaded = true;
    }

    rv = dm_mngr_load_controllers("SupportedLEDControllers", mod_dir);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load all SupportedLEDControllers");
        ethernet.led_mods_are_loaded = false;
        rv = dm_mngr_load_dummy(mod_dir, DUMMY_LED_MOD_NAME);
        when_failed_trace(rv, exit, ERROR, "Failed to load dummy LEDController");
    } else {
        ethernet.led_mods_are_loaded = true;
    }

    rv = dm_mngr_load_controllers("SupportedRMONStatsControllers", mod_dir);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load all SupportedRMONStatsControllers");
        rv = 0;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int ethernet_clean(void) {
    SAH_TRACEZ_IN(ME);
    ethernet.dm = NULL;
    ethernet.parser = NULL;
    ethernet.netdev_ctx = NULL;

    amxm_close_all();
    netmodel_cleanup();

    SAH_TRACEZ_INFO(ME, "Ethernet manager stop");

    SAH_TRACEZ_OUT(ME);
    return 0;
}

amxd_dm_t* PRIVATE ethernet_get_dm(void) {
    return ethernet.dm;
}

amxb_bus_ctx_t* PRIVATE ethernet_get_netdev_ctx(void) {
    return ethernet.netdev_ctx;
}

amxo_parser_t* PRIVATE ethernet_get_parser(void) {
    return ethernet.parser;
}

bool PRIVATE ethernet_vlan_mods_are_loaded(void) {
    return ethernet.vlan_mods_are_loaded;
}

bool PRIVATE ethernet_led_mods_are_loaded(void) {
    return ethernet.led_mods_are_loaded;
}

int _ethernet_manager_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    SAH_TRACEZ_INFO(ME, "entry point %s, reason: %d", __func__, reason);

    switch(reason) {
    case AMXO_START:
        rv = ethernet_init(dm, parser);
        break;
    case AMXO_STOP:
        rv = ethernet_clean();
        break;
    }

    SAH_TRACEZ_OUT(ME);
    return rv;
}
